import os
import sys
import urllib.request, requests
import re

from icle import IcleSqlite, IcleConfig, IcleLogger
from icle.icle_sqlite.icle_sqlite import Validator
from datetime import datetime

import fitz

from lxml import html

from itertools import groupby


base_url = "https://www.ivass.it/"
entity_type_regular = re.compile(r"(?P<entity_type>s\.p\.a\.|s\.r\.l\.)")
code_regular = re.compile(r"\(.*(?P<code>[A-Z]\d+)")
address_regular = r"\n?con \n?sede \n?in \n?(?P<address>\w+)"
splitter_regular = re.compile(r"(?P<details>(?:ORDINANZA|PROVVEDIMENTO).*PROT. N.*\n)")
details_regular = re.compile(r"(?:Violazione|Norma Sanzionatoria|Importo sanzione|Sanzione)")
name_regular = re.compile(r"Destinatario[\n\s]+(?P<name>.*?)(?:(?:\n?con \n?sede \n?in \n?(?:\w+))|(?:\(.*(?:[A-Z]\d+))|,)")
date_regular = re.compile(r"(?P<date>\d{1,2} \w+ \d{4})")
dotter_regular = r"\s+\.+\s+(?P<page_num>\d+)"
first_headline = r"SANZIONI AMMINISTRATIVE PECUNIARIE: ORDINANZE"
second_headline = r"SANZIONI DISCIPLINARI: RADIAZIONI\s?-?\s?INTERMEDIARI"


def parse_date(date):
    month_date = {"gennaio": "1", "febbraio": "2", "marzo": "3", "aprile": "4", "maggio": "5", "giugno": "6" , "luglio": "7", "agosto": "8", "settembre": "9", "ottobre": "10", "novembre": "11", "dicembre": "12"}
    date = date.lower().split()
    date[1] = month_date[date[1]]
    return datetime.strptime(".".join(date), "%d.%m.%Y").strftime("%d.%m.%Y")


def get_year_urls(url):
    page = requests.get(url)
    tree = html.fromstring(page.content)
    xpath = "//div[@class='archive-links']//ol//a"
    elements = tree.xpath(xpath)
    return [base_url + element.attrib['href'] for element in elements] 


def get_next_page(tree):
    xpath = ".//div[@class='ivass-paginator']//span[contains(text(), ' » ')]/.."
    element = tree.xpath(xpath)
    next_page_url = base_url + element[0].attrib['href'] if element[0].attrib.get('href') else None
    return next_page_url


def get_month_urls(url):
    year_urls = get_year_urls(url)
    urls = []
    for year_url in year_urls:
        page = requests.get(year_url)
        tree = html.fromstring(page.content)
        xpath = "//a//span[@class='link-title']/.."
        elements = tree.xpath(xpath)
        urls.extend([base_url + element.attrib['href'] for element in elements])
        next_page_url = get_next_page(tree)
        if next_page_url:
            year_urls.append(next_page_url)
    return urls


def get_urls_download_file(url):
    month_urls = get_month_urls(url)
    urls = []
    for month_url in month_urls:
        page = requests.get(month_url)
        tree = html.fromstring(page.content)
        xpath = "//h2[contains(text(), 'Altri documenti')]/..//span[@class='link-title' and contains(text(), 'Appendice')]/.."
        element = tree.xpath(xpath)
        if element:
            urls.append(base_url + element[0].attrib['href']) 
    return urls


def get_download_files(url):
    download_urls = get_urls_download_file(url)
    for download_url in download_urls:
        urllib.request.urlretrieve(download_url, f"{current_folder}/resources/file.pdf")
        yield download_url


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people:
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        for address in person_dictionary["address"]:
            person.add_address(city=address, street='', zip='', region='', address_type='OPERATIONAL ADDRESS', country='International')
        if person_dictionary["entity_type"]:
            person.entity_type = "L"
        else:
            person.entity_type = "P"    
        for code in person_dictionary["codes"]:
            if code:
                person.add_identifier("IVASS:" + code)
        person_specifications = person_dictionary["specifications"]
        for person_specification in person_specifications:
            specification = icle_sqlite.create_specification()
            specification.list_pub_date = parse_date(person_specification["pub_date"])
            specification.add_person(person)    
            specification.add_rationale(person, "Sanctions", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")
 

def parse_pages(pdf, first_page, page_num_start, page_num_end):
    people = []
    for page_num in range(int(page_num_start)+first_page-1, int(page_num_end)+first_page-1):
        page = pdf.load_page(page_num)
        text = page.get_text("text").strip()
        sub_headlines = re.split(splitter_regular, text)
        details = re.search(splitter_regular, text) 
        details = details.group("details") if details else None
        if details:
            date = re.search(date_regular, text).group("date") 
            for i in range(1, len(sub_headlines), 2):
                text = sub_headlines[i] + sub_headlines[i+1]
                details += "".join(re.split(details_regular, text)[1:])
                name = re.search(name_regular, text)
                if name:
                    name = name.group("name").strip()
                    entity_type = re.search(entity_type_regular, text)
                    address = re.search(address_regular, text)
                    code = re.search(code_regular, text)
                    entity_type = entity_type.group("entity_type").strip() if entity_type else ""
                    address = address.group("address").strip() if address else ""
                    code = code.group("code") if code else ""
                    people.append({"name": name, "entity_type": entity_type, "pub_date": date, "address": address, "details": details, "code": code})
    return people


def group_by_name(people):
    people = sorted(people, key=lambda item: item['name'].lower())
    grouped_people = []
    for name, grouped_person in groupby(people, key=lambda item: item['name'].lower()):
        people = {}
        for person in grouped_person:
            if not "name" in people:
                people["name"] = person["name"]
            if not "codes" in people:
                people["codes"] = set()
            if not "address" in people:
                people["address"] = set()
            if not person["code"] in people["codes"]:
                people["codes"].add(person["code"])
            if not person["address"] in people["address"]:
                people["address"].add(person["address"])
            people["entity_type"] = person["entity_type"]
            if not "specifications" in people:
                people["specifications"] = []
            people["specifications"].append({"pub_date": person["pub_date"], "details": person["details"]})    
        grouped_people.append(people)
    return grouped_people


def parse_objects(url):
    people = []
    download_files_generator = get_download_files(url)
    for download_url in download_files_generator:
        logger.log(logger.LogLevels.INFO, f"Reading downloaded pdf file with url: {download_url}")
        pdf = fitz.open(f"{current_folder}/resources/file.pdf")
        pdf_text = ""
        first_page = None
        for current_page in range(len(pdf)):
            page = pdf.load_page(current_page)
            current_page_text = page.get_text("text")
            if not first_page and bool(re.search(r"\s+I\s+", current_page_text)):
                first_page = current_page
            pdf_text += current_page_text
        page_numbers = re.findall(r"[:\w\s]+" + dotter_regular, pdf_text)
        first_headline_page_num_start = re.search(first_headline + dotter_regular, pdf_text)
        first_headline_page_num_start = first_headline_page_num_start.group("page_num") if first_headline_page_num_start else None
        second_headline_page_num_start = re.search(second_headline + dotter_regular, pdf_text)
        second_headline_page_num_start = second_headline_page_num_start.group("page_num") if second_headline_page_num_start else None
        first_headline_page_num_end = len(pdf) - first_page
        second_headline_page_num_end = len(pdf) - first_page

        for i in range(len(page_numbers)):
            if i+1 < len(page_numbers):
                if first_headline_page_num_start and page_numbers[i] == first_headline_page_num_start and page_numbers[i+1] != first_headline_page_num_start:
                    first_headline_page_num_end = page_numbers[i+1]
                if second_headline_page_num_start and page_numbers[i+1] != second_headline_page_num_start:
                    second_headline_page_num_end = page_numbers[i+1]    

        if first_headline_page_num_start:
            people.extend(parse_pages(pdf, first_page, first_headline_page_num_start, first_headline_page_num_end))
        if second_headline_page_num_start:
            people.extend(parse_pages(pdf, first_page, second_headline_page_num_start, second_headline_page_num_end))
    people = group_by_name(people) 
    return people


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "2654"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")  
