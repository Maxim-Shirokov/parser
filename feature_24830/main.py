import os
import sys
import re
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger
from datetime import datetime

from bs4 import BeautifulSoup


possible_name_regular = re.compile(r"(?:(?:impose(?:d)?\s*on\s*(?:the|a)?|institution|(?:<strong>)?public reprimand(?:<\/strong>)? of)\s*)<strong>(?P<name>.*?)<\/strong>\s*(<strong>(?P<name_addition>.*?)<\/strong>)?")
splitter = re.compile(r"(,\s*in (?:his|her|it|its) capacity|,\s*who|,.*director|\(.*\)|of\s*disqualification)")
possible_name_regular2 = re.compile(r"(?:mr|ms).*", re.IGNORECASE)
alias_regular = re.compile(r"(?P<alias>\(.*\))")


def parse_date(date):
    return datetime.strptime(date, "%d/%m/%y").strftime("%d.%m.%Y")


def get_name_and_alias(element):
    possible_name = re.search(possible_name_regular, str(element))
    name_addition = (
        possible_name.group("name_addition") if possible_name and possible_name.group("name_addition") else ""
    )
    possible_name = possible_name.group("name") + " " + name_addition if possible_name else ""
    if not possible_name:
        possible_name_element = element.find("strong", text=possible_name_regular2)
        possible_name = possible_name_element.text if possible_name_element else ""
        if possible_name.lower().strip() in ('ms', 'mr'):
            possible_name += " " + possible_name_element.find_next_sibling("strong").text
    name = re.split(splitter, possible_name)[0]
    alias = re.search(alias_regular, possible_name)
    alias = alias.group("alias") if alias else ""
    return ' '.join(name.split()).strip().removesuffix("."), alias.strip()


def parse_objects(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    table = soup.find("table", {"summary": "Information on infringements and sanctions imposed"})
    rows = table.find_all("tr", {"class": "jcorgFilterTextParent"})
    people = {}
    for row in rows:
        columns = row.find_all("td")
        pub_date = parse_date(columns[0].text)
        start_date = parse_date(columns[2].text)
        details = columns[1].getText().strip() + "\n" + columns[3].getText().strip()
        name, alias = get_name_and_alias(columns[1])
        if not name:
            continue
        name_lower = name.lower()
        if not name_lower in people:
            people[name_lower] = {"name": name, "specifications": [], "alias": alias}
        if alias:
            people[name_lower]["alias"] = alias
        people[name_lower]["specifications"].append(
            {"start_date": start_date, "details": details, "pub_date": pub_date}
        )
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        person.alias = person_dictionary["alias"]
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.list_pub_date = person_specification["pub_date"]
            specification.add_person(person)
            specification.start_date = person_specification["start_date"]
            specification.add_rationale(person, "Sanctions", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "24830"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
