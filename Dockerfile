FROM python:3.9

RUN mkdir -p /app/

COPY icle-1.0.1.tar.gz /app/
COPY requirements.txt /app/

RUN apt-get update
RUN apt-get -y install libc-dev  gcc libxslt-dev g++ 

RUN cd /app/ && \
    pip install ./icle-1.0.1.tar.gz && \
    pip install -r requirements.txt

COPY . /app/

WORKDIR /app/
