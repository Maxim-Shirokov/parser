import os
import re
import sys
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger
from datetime import datetime

from bs4 import BeautifulSoup


base_url = "https://acpr.banque-france.fr"

name_part_regexp = re.compile(r"(.*?)\s*(?:(?:n°?\s*(?:\d+-\d+))?).*?(?:(?:du|rendue le)\s+(?:[\der]+ [\wé]+ \d+)).?\s+.*?(?P<name_part>.*?)$")
sanction_number_regexp = re.compile(r"sanctions\s*n°\s*(?P<sanction_number>\d+-\d+)")
pub_date_regexp = re.compile(r"\s+du\s*(?P<pub_date>[\der]+ [\wé]+ \d+)")
name_regexp1 = re.compile(
    r"(?:((?:égard|encontre) (?:de|d’|d')|par la)\s*(?:la)?\s?(?:.*?société)?)(?P<name>.*?)(?:\(|,|–|dans le|contre|d'Atlantique|$)",
    re.IGNORECASE,
)
name_regexp2 = re.compile(r"(?P<name>.*?)(?:\(.*)?(?:$|;|,)", re.IGNORECASE)
name_regexp3 = re.compile(r"(?P<name>.*?)(du|-|–)")
details_regexp = re.compile(r"\((?P<details>.*?)\)")
details_regexp2 = re.compile(r"–(?P<details>(.*))")
month_date = {
    "janvier": "1",
    "février": "2",
    "mars": "3",
    "avril": "4",
    "mai": "5",
    "juin": "6",
    "juillet": "7",
    "août": "8",
    "septembre": "9",
    "octobre": "10",
    "novembre": "11",
    "décembre": "12",
}


def parse_date(date):
    date = date.replace("1er", "1")
    date = date.lower().split(" ")
    date[1] = month_date[date[1]]
    return datetime.strptime(".".join(date), "%d.%m.%Y").strftime("%d.%m.%Y")


def get_cards(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    cards = soup.find("div", {"id": "paragraphs-item--38835"}).find_all("div", {"class": "field-item even rich-text"})
    return cards


def get_pdf_url(url):
    return url.get("href") if url.get("href").startswith(base_url) else base_url + url.get("href")


def parse_objects(url):
    people = {}
    cards = get_cards(url)
    for card in cards:
        for month in card.find_all("h2"):
            for next_sibling in month.next_siblings:
                if next_sibling.name == "h2":
                    break
                if next_sibling.name == "ul":
                    pdfs = []
                    details = ""
                    if "margin-left:" not in next_sibling.get("style", ""):
                        for li in next_sibling.find_all("li"):
                            url = li.find("a", href=lambda href: href and ".pdf" in href)
                            text = li.get_text()
                            pdfs = [get_pdf_url(url)]
                            name_part = re.search(name_part_regexp, text)
                            name_part = name_part.group("name_part") if name_part else ''
                            name = re.search(name_regexp1, name_part)
                            sanction_number = re.search(sanction_number_regexp, text)
                            sanction_number = sanction_number.group("sanction_number") if sanction_number else ''
                            pub_date = re.search(pub_date_regexp, text)
                            pub_date = parse_date(pub_date.group("pub_date")) if pub_date else None
                            details = re.findall(details_regexp, text)
                            details = '\n'.join(details).strip()
                            if not details:
                                details = re.search(details_regexp2, text)
                                details = details.group("details") if details else ''
                            if sub_ul := li.find("ul"):
                                pdfs.extend([get_pdf_url(a) for a in sub_ul.find_all("a", href=lambda href: href and ".pdf" in href)])
                            if name:
                                name = name.group("name")
                            else:
                                name = re.search(name_regexp2, name_part)
                                name = name.group("name") if name else None
                            if not name:
                                name = re.search(name_regexp3, text)
                                name = name.group("name") if name else ''
                    else:
                        pdfs = [get_pdf_url(a) for a in next_sibling.find_all("a", href=lambda href: href and ".pdf" in href)]
                    name = name.strip().replace('\xa0', ' ')
                    name_lower = name.lower()
                    details = f"sanction_number: {sanction_number}\n{details}" if sanction_number else details
                    if not name_lower in people:
                        people[name_lower] = {"name": name, "specifications": []}
                    people[name_lower]["specifications"].append({"pdfs": pdfs, "pub_date": pub_date, "details": details})
                if next_sibling.name == "p":
                    text = next_sibling.get_text()
                    details2 = re.search(details_regexp, text)
                    if details2:
                        people[name_lower]["specifications"].append({"details": details2.group('details')})

    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            for pdf in person_specification.get("pdfs", []):
                specification.set_and_download_url_doc(pdf)
            if person_specification.get("pub_date"):
                specification.list_pub_date = person_specification["pub_date"]
            specification.add_rationale(person, "Sanctions", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25267"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
    # parse_objects("https://acpr.banque-france.fr/sanctionner/recueil-de-jurisprudence")
