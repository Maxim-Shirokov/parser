import os
import re
import sys
import urllib.request, requests
from datetime import datetime

from icle import IcleSqlite, IcleConfig, IcleLogger

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from bs4 import BeautifulSoup


base_url = "https://www.publicsafety.gc.ca/"
xml_url = "{http://www.w3.org/2005/Atom}"
alias_regular = re.compile(r"(?P<alias>\(.*?\))")
base_url = "https://www.nationalsecurity.gov.au"
conclusion_regular = re.compile("Conclusion")
alias_regular_in_details = re.compile("Also known as")
details_of_the_organisation_regular = re.compile("Details of the organisation")
links_to_other_terrorist_organisations_regular = re.compile("Links to other terrorist organisations")


def parse_date(date):
    return datetime.strptime(date, '%Y-%m-%d').strftime("%d.%m.%Y")


def get_xml(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    tag_a = soup.find("a", href=lambda href: href and ".xml" in href)
    return base_url + tag_a.get('href')


def get_download_file(url):
    download_url = get_xml(url)
    urllib.request.urlretrieve(download_url, f"{current_folder}/resources/file.xml")
    return download_url


def get_name_and_alias(element):
    name = " ".join(element.text.split()).replace(u"\u200b", "")
    alias = re.search(alias_regular, name)
    alias = alias.group("alias") if alias else ""
    return name, alias


def get_pub_date(element):
    pub_date = re.search(r"(?P<date>\d{1,2}.*?\d{1,4})", element.text)
    pub_date = pub_date.group("date")
    return datetime.strptime(pub_date, "%d %B %Y").strftime("%d.%m.%Y")


def parse_detail(url, driver):
    driver.get(url)
    WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, "Conclusion")))
    html = driver.page_source
    soup = BeautifulSoup(html, "html.parser")
    conclusion_id = soup.find("a", text=conclusion_regular).get("href").replace("#", "")
    details_of_the_organisation_id = (
        soup.find("a", text=details_of_the_organisation_regular).get("href").replace("#", "")
    )
    conclusion = soup.find(id=conclusion_id).find("div", {"class": "edit-text"}).get_text()
    details_of_the_organisation = (
        soup.find(id=details_of_the_organisation_id).find("div", {"class": "edit-text"}).get_text()
    )
    links_to_other_terrorist_organisations = soup.find("h4", text=links_to_other_terrorist_organisations_regular)
    links_to_other_terrorist_organisations = (
        links_to_other_terrorist_organisations.find_next("p").get_text()
        if links_to_other_terrorist_organisations
        else ""
    )
    alias = soup.find(text=alias_regular_in_details)
    if alias:
        parent = alias.find_parent("p")
        alias = (
            parent.get_text().replace("Also known as", "") if parent else alias.get_text().replace("Also known as", "")
        )
        alias = alias.replace(u"\u200b", "")
        alias = alias.replace(u":", "")
        alias = alias.replace(u'\xa0', u' ')
    details = conclusion + "\n" + details_of_the_organisation + "\n" + links_to_other_terrorist_organisations
    return alias, details


def parse_objects(url):
    options = Options()
    options.add_argument("--headless")
    driver = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"), options=options)
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    table_body = soup.find("table").find("tbody")
    rows = table_body.find_all("tr")
    people = {}
    for row in rows:
        columns = row.find_all("td")
        name, alias = get_name_and_alias(columns[0])
        pub_date = get_pub_date(columns[1])
        details = columns[2].text.strip()
        detail_url = base_url + columns[0].find("a").get("href")
        added_alias, added_details = parse_detail(detail_url, driver)
        alias = alias + ";" + added_alias if alias else added_alias
        details += "\n" + added_details
        details = details.replace(u"\u200b", "")
        details = details.replace(u'\xa0', u' ')
        name_lower = name.lower()
        if not name_lower in people:
            people[name_lower] = {"name": name, "alias": alias, "specifications": []}
        people[name_lower]["specifications"].append({"details": details, "pub_date": pub_date})
    driver.close()
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        person.alias = person_dictionary["alias"]
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.list_pub_date = person_specification["pub_date"]
            specification.add_person(person)
            specification.add_rationale(person, "Terrorism", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25331"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
