import os
import re
import sys
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger
from datetime import datetime

from bs4 import BeautifulSoup


name_regexp = re.compile(r"^(?P<name>.*?)(\((?:aka|also|AKA):?(?P<alias>.*?)\))?,(?P<address>.*)")
address_regexp = re.compile(r"^(?P<street>.*?),(?P<city>.*?),(?P<country>.*?)\s+(?P<zip>[\d\s]+)")
zip_regexp = re.compile(r"(?P<zip>(?:\d\s?)+)")

common_details = "This is the U.S. Department of State’s Cuba Prohibited Accommodations List, a list of properties in Cuba owned or controlled by the Cuban government, a prohibited official of the Government of Cuba, as defined in 31 CFR § 515.337, a prohibited member of the Cuban Communist Party, as defined in 31 CFR § 515.338, a close relative, as defined in 31 CFR § 515.339, of a prohibited official of the Government of Cuba, or a close relative of a prohibited member of the Cuban Communist Party"


def parse_address(address, region):
    split_address = []
    split_region = re.split(region.lower().replace("í", "i").replace("ü", "u").replace("á", "a"), address.lower().replace("í", "i").replace("ü", "u").replace("á", "a"))
    for i in re.split(f",|{region}|Cuba", address)[::-1]:
        if i := i.strip():
            split_address.append(i.replace("\xa0", " "))
    zip_code = re.search(zip_regexp, split_address[0]).group("zip").replace("\u202f", "")
    
    if len(split_region) == 1:
        street = ' '.join(split_address[0:]).replace("\u202f*", "").replace("\u202f^", "").replace(zip_code, "")
    else:    
        street = ' '.join(split_address[1:]).strip() if len(split_region) == 2 else ' '.join(split_address[2:])
    street = re.sub(r"^\.", "", street).strip()    
    
    if len(split_region) == 2 or (len(split_region) == 1 and len(split_address) == 1):
        city = region.replace(zip_code, "")
    else:
        city = split_address[1].strip()
    return street, city, zip_code     
        


def parse_objects(url):
    people = {}
    page = requests.get(url, verify=False)
    soup = BeautifulSoup(page.text, 'html.parser')
    div = soup.find("div", {"class": "entry-content"})
    elements = list(div.find_all(recursive=False))[3:]
    region = ''
    for element in elements:
        if element.name == "h5":
            region = element.get_text().replace(":", "")
        else:
            info = re.search(name_regexp, element.get_text())
            if info:
                name = info.group("name").strip().replace(u'\xa0', u' ').replace("\u202f", "*")
                alias = info.group("alias").strip() if info.group("alias") else ''
                address = info.group("address")
                street, city, zip_code = parse_address(address, region)
                name_lower = name.lower()
                if not name_lower in people:
                    people[name_lower] = {"name": name, "aliases": [], "addresses": [], "specifications": []}
                if alias:
                    people[name_lower]["aliases"].append(alias.strip().replace(u'\xa0', u' '))
                people[name_lower]["addresses"].append({"region": region, "street": street, "city": city, "country": "Cuba", "zip": zip_code})
                people[name_lower]["specifications"].append({"details": common_details})
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        if person_dictionary["aliases"]:
            person.alias = "; ".join(person_dictionary["aliases"])
        for address in person_dictionary["addresses"]:
            # add_address(self, street, zip, city, region, country, address_type)
            person.add_address(address["street"], address["zip"], address["city"], address["region"], "Cuba", "OTHER ADDRESS")
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            specification.add_rationale(person, "Cuba Restricted List", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25552"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
