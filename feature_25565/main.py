from datetime import datetime
import os
import re
import sys
import urllib.request, requests

from icle import IcleSqlite, IcleConfig, IcleLogger

from bs4 import BeautifulSoup

import xml.etree.ElementTree as ET


base_url = "https://laws-lois.justice.gc.ca"
xml_url = "{http://justice.gc.ca/lims}"
name_alias_regexp = re.compile(r"(?P<name>.*?)(?:\(|,)(?:also known among other names as(?P<alias>.*?)\))?")
place_date_birth_regexp = re.compile(r"born\s+on\s+(?P<birth_date>\w+\s+\d{1,2}[,\s]+\d{4})[,\s]+in\s+(?P<birth_place>.*?),")


def parse_date(date):
    date = date.replace("\xa0", " ").replace(",", "")
    return datetime.strptime(date, '%B %d %Y').strftime("%d.%m.%Y")


def get_xml(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    tag_a = soup.find("a", href=lambda href: href and ".xml" in href)
    return base_url + tag_a.get('href')


def get_download_file(url):
    download_url = get_xml(url)
    urllib.request.urlretrieve(download_url, f"{current_folder}/resources/file.xml")
    return download_url


def parse_objects(url):
    download_url = get_download_file(url)
    logger.log(logger.LogLevels.INFO, f"Reading downloaded pdf file with url: {download_url}")
    people = {}
    tree = ET.parse(f"{current_folder}/resources/file.xml")
    root = tree.getroot()
    list = root.find("Schedule").find("List")
    for item in list.iter("Item"):
        text = item.find("Text").text
        if text:
            start_date = item.get(f"{xml_url}inforce-start-date")
            name_alias = re.search(name_alias_regexp, text)
            name = name_alias.group("name").strip()
            alias = name_alias.group("alias")
            place_date_birth = re.search(place_date_birth_regexp, text)
            birth_place = place_date_birth.group("birth_place").strip().replace('Tunis', 'Tunisia').replace('\xa0', ' ') if place_date_birth else None
            birth_date = parse_date(place_date_birth.group("birth_date").strip()) if place_date_birth else None
            details = f"{text}\nlims fid: {item.get(f'{xml_url}fid')}\nlims id: {item.get(f'{xml_url}id')}".strip().replace('\xa0', ' ')
            name_lower = name.lower()
            if name_lower not in people:
                people[name_lower] = {"name": name, "birth_places": [], "birth_dates": [], "specifications": []}
            if alias:
                people[name_lower]["alias"] = alias.strip()
            if birth_place:
                people[name_lower]["birth_places"].append(birth_place)
            if birth_date:
                people[name_lower]["birth_dates"].append(birth_date)
            people[name_lower]["specifications"].append({"details": details, "start_date": start_date})
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        if person_dictionary.get("alias"):
            person.alias = person_dictionary["alias"]
        for birth_date in person_dictionary["birth_dates"]:
            person.add_birth_date(birth_date)
        for birth_place in person_dictionary["birth_places"]:
            person.add_birth_place(city='', region='', country_name=birth_place)
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.start_date = person_specification["start_date"]
            specification.add_person(person)
            specification.add_rationale(person, "Freezing Assets of Corrupt Foreign Officials (Tunisia) Regulations", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25565"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
