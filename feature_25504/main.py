from datetime import datetime
import os
import re
import sys
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger

from bs4 import BeautifulSoup

from lxml import html

base_url = "https://www.cbsa-asfc.gc.ca"
birth_regular = re.compile(r"(\d{4}-\d{2}-\d{2})")


def parse_date(date):
    return datetime.strptime(date, '%Y-%m-%d').strftime("%d.%m.%Y")


def get_cards(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    cards = soup.find_all("div", {"class": "well"})
    return cards


def get_alias(card):
    alias = ""
    siblings = card.find("b", text="Alias:").next_siblings
    for sibling in siblings:
        alias += sibling.text.strip()
    return alias


def get_birth_dates(card):
    birth_dates = card.find("b", text="Date of Birth:").next_sibling.strip()
    return re.findall(birth_regular, birth_dates)


def get_gender(card):
    gender = card.find("b", text="Gender:").next_sibling.text.strip()
    if "Male" in gender:
        return "M"
    if "Female" in gender:
        return "F"
    return "NA"


def parse_date(date):
    return datetime.strptime(date, '%Y-%m-%d').strftime("%d.%m.%Y")


def get_image(card):
    src = card.find("img").get("src")
    if not "/wc-cg/" in src:
        src = "/wc-cg/" + src
    return base_url + src    


def parse_objects(url):
    cards = get_cards(url)
    people = {}
    for card in cards:
        name = card.find("p", {"class": "lead"}).text.strip()
        image = get_image(card)
        alias = get_alias(card)
        gender = get_gender(card)
        birth_dates = get_birth_dates(card)
        birth_place = card.find("b", text="Place of Birth:").next_sibling.strip()
        details = card.find("b", text="Identifying Features:").find_next("p").get_text().strip()
        name_lower = name.lower()
        if name_lower not in people:
            people[name_lower] = {
                "name": name,
                "image": image,
                "alias": alias,
                "gender": gender,
                "birth_dates": birth_dates,
                "birth_place": birth_place,
                "specifications": [],
            }
        people[name_lower]["specifications"].append({"details": details})

    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        person.alias = person_dictionary["alias"]
        person.gender = person_dictionary["gender"]
        person.add_birth_place(city=None, region=None, country_name=person_dictionary["birth_place"])
        person.set_and_download_url_image(person_dictionary["image"])
        for birth_date in person_dictionary["birth_dates"]:
            person.add_birth_date(parse_date(birth_date))
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            specification.add_rationale(person, "Most Wanted", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25504"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
