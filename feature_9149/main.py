import sys
import os, urllib.request, requests
import json
import re
from os import listdir
from datetime import datetime

from icle import IcleSqlite, IcleConfig, IcleLogger
from icle.icle_sqlite.icle_sqlite import Validator

from lxml import html
from collections import deque


def get_next_page_url(tree):
    xpath = ".//a[@class='next page-numbers']"
    element = tree.find(xpath)
    next_page_url = element.attrib['href'] if element else None
    return next_page_url


def parse_date(date):
    return datetime.strptime(date, '%B %d, %Y').strftime("%d.%m.%Y")
        

def parse_objects(url):
    full_dictonary = []
    next_page_urls = deque([url])
    name_details_regexp = re.compile(r'MFSA Warning –(?P<name>.*)–(?P<detail>.*)')
    while next_page_urls:
        url = next_page_urls.popleft()
        page = requests.get(url)
        tree = html.fromstring(page.content)
        xpath = ".//div[@class='single-news-item']"
        elements = tree.findall(xpath)
        for element in elements:
            date = element.find(".//div[@class='date-published']").text.strip()
            date = parse_date(date)
            title = element.find(".//a[@class='title-link ']").text.strip()
            name_detail = name_details_regexp.search(title)
            name = name_detail.group("name").strip() if name_detail else ""
            detail = name_detail.group("detail").strip() if name_detail else ""
            if name:
                full_dictonary.append({"name": name, "date": date, "detail": detail})
        next_page_url = get_next_page_url(tree)
        if next_page_url:
            next_page_urls.append(next_page_url)
    return full_dictonary


def create_objects(full_dictionary):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")   
    
    for dictionary in full_dictionary:
        logger.log(logger.LogLevels.INFO, f"Saving element {str(dictionary)}")  
        person = icle_sqlite.create_person()
        person.full_name = dictionary["name"]
        specification = icle_sqlite.create_specification()
        pub_date = dictionary["date"]
        if pub_date != "":
            specification.list_pub_date = pub_date
        specification.add_person(person)
        specification.add_rationale(person, "Warnings", dictionary["detail"])
    logger.log(logger.LogLevels.INFO, f"Saving file")  
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    elements = parse_objects(icle_config.link)
    create_objects(elements)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "9149"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
