import os
import re
import sys
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger

from lxml import html


name_details_regular = re.compile(r"Nombre:(?P<name>(?:.|\n)*)Delito:(?P<details>(?:.|\n)*)")
base_url = "https://www.policia.es/"


def get_cards(url):
    page = requests.get(url)
    tree = html.fromstring(page.content)
    xpath = "//div[@class='card-deck']//div[@class='card bg-white border-0']"
    cards = tree.xpath(xpath)
    return cards


def get_image(card):
    img = card.xpath(".//img[@class='card-img-top img-fluid']")
    return base_url + img[0].attrib["src"] if img else ""


def get_name_and_details(card):
    text = card.find(".//div[@class='card-body']").text_content()
    groups = re.search(name_details_regular, text)
    name = groups.group("name").strip()
    details = groups.group("details").strip()
    return name, details


def parse_objects(url):
    people = {}
    cards_generator = get_cards(url)
    for card in cards_generator:
        name, details = get_name_and_details(card)
        image = get_image(card)
        name_lower = name.lower()
        if not name_lower in people:
            people[name_lower] = {"name": name, "details": "", "image": image}
        people[name_lower]["details"] += f"\n{details}"
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        person.set_and_download_url_image(person_dictionary["image"])
        specification = icle_sqlite.create_specification()
        specification.add_person(person)
        specification.add_rationale(person, "Most Wanted", person_dictionary["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "24770"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")

