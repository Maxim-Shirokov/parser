import os
import re
import sys
import time
import urllib.request, requests
from datetime import datetime

from icle import IcleSqlite, IcleConfig, IcleLogger

from selenium import webdriver
import selenium
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

from bs4 import BeautifulSoup


def parse_date(date):
    return datetime.strptime(date, '%d %b %Y').strftime("%d.%m.%Y") if date else None


def get_page_numbers():
    pagination_numbers = WebDriverWait(driver, 120).until(EC.visibility_of_all_elements_located((By.CLASS_NAME, "pagination__numbers")))
    return int(pagination_numbers[1].text.strip())


def pagination_submit_click(page_number):
    element = WebDriverWait(driver, 120).until(EC.visibility_of_element_located((By.ID, "-pagination-page")))
    element.clear()
    element.send_keys(f"{page_number}")
    driver.find_element_by_id("-pagination-submit").click()


def get_pdf(details_element):
    try:
        pdf = WebDriverWait(details_element, 1).until(EC.presence_of_element_located((By.TAG_NAME, "a"))).get_attribute("href")
        return pdf
    except selenium.common.exceptions.TimeoutException:
        return None


def get_more_information(link):
    window_before = driver.window_handles[0]
    driver.execute_script(f"window.open('{link}');")
    window_after = driver.window_handles[1]
    driver.switch_to.window(window_after)
    specifications = []
    table_body = WebDriverWait(driver, 120).until(EC.visibility_of_element_located((By.XPATH, "//table[contains(@class, 'tablesaw-stack')]//tbody")))
    ActionChains(driver).move_to_element(table_body).perform()
    rows = table_body.find_elements_by_xpath(".//tr[contains(@id, 'disciplinary-history-row')]")
    for i, row in enumerate(rows):
        details_element = row.find_element_by_id(f"history-list-details-{i}")
        details = re.sub("\s+", " ", row.find_element_by_id(f"history-list-details-{i}Hidden").get_attribute("textContent").strip())
        start_date = parse_date(row.find_element_by_id(f"history-list-date-{i}").text)
        pdf = get_pdf(details_element)
        specifications.append({"details": details, "start_date": start_date, "pdf": pdf})
    driver.execute_script(f"window.open('','_self').close();") 
    driver.switch_to.window(window_before)   
    return specifications


def parse_objects(url):
    people = {}
    driver.get(url)
    page_numbers = get_page_numbers()
    for page_number in range(1, page_numbers + 1):
        logger.log(logger.LogLevels.INFO, f"Page: {page_number}")
        cards = WebDriverWait(driver, 120).until(
            EC.visibility_of_all_elements_located((By.XPATH, "//div[@class='slds-col ']/ol[@class='results individual-results stack stack--direct stack--x-small ']/li"))
        )
        for i in range(len(cards)):
            card = WebDriverWait(driver, 120).until(EC.visibility_of_element_located((By.ID, f"individual-result-card-{i}")))
            ActionChains(driver).move_to_element(card).perform()
            tag_a = WebDriverWait(driver, 120).until(EC.visibility_of_element_located((By.ID, f"individual-result-card-{i}_main-name")))
            link = tag_a.get_attribute("href")
            name = tag_a.text.strip()
            code = card.find_element_by_id(f"individual-result-card-{i}_main-reference").text.strip()
            badge = card.find_element_by_id(f'individual-result-card-{i}_badge-2')
            badge = driver.execute_script("""var parent = arguments[0]; var child = parent.firstChild; var ret = ""; while(child) {if (child.nodeType === Node.TEXT_NODE) ret += child.textContent; child = child.nextSibling;} return ret;""", badge)
            card_content = card.find_element_by_id(f'individual-result-card-{i}_content').text.strip().replace('View the record for all connected firms and roles', '')
            details = re.sub("\s+", " ", f"badge: {badge}\ncode: {code}\n{card_content}")
            specifications = get_more_information(link)
            specifications.append({"details": details})
            name_lower = name.lower()
            if not name_lower in people:
                people[name_lower] = {"name": name, "specifications": []}
            people[name_lower]["specifications"].extend(specifications)
            logger.log(logger.LogLevels.INFO, f"Collected element {str(people[name_lower])}")
        pagination_submit_click(page_number + 1)
    driver.close()    
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            if person_specification.get("start_date"):
                specification.start_date = person_specification["start_date"]
            if person_specification.get("pdf"):
                try:
                    specification.set_and_download_url_doc(person_specification["pdf"])
                except (requests.exceptions.ConnectionError, requests.exceptions.InvalidURL):
                    logger.log(logger.LogLevels.INFO, f"Bad pdf url")    
            specification.add_rationale(person, "Prohibitions", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    driver = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"))
    DEBUG = False
    feature_code = "24538"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
