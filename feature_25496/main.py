import os
import re
import sys
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger
from datetime import datetime

from bs4 import BeautifulSoup


def parse_date(date):
    date = date.replace("-", " ")
    return datetime.strptime(date, "%d %b %Y").strftime("%d.%m.%Y")


def parse_address(address):
    br_tags = [x for br in address if (x := re.sub(r"\s+", " ", br.get_text().strip()))]
    country = br_tags[-1].strip()
    address = ' '.join(br_tags[:len(br_tags) - 1])
    if country == "Unknown":
        country = icle_config.authority_country
    return country, address


def get_first_table(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    h2 = soup.find("h2", text=lambda text: text and "EBRD-initiated sanctions" in text)
    table = h2.find_next_sibling("table")
    return table


def get_second_table(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    h2 = soup.find("strong", text=lambda text: text and "Sanctions resulting from third party findings" in text).find_parent("h2").find_next("div")
    table = h2.table
    return table


def parse_objects_from_first_table(url):
    people = {}
    table = get_first_table(url)
    rows = table.find_all("tr")[2:]
    for row in rows:
        columns = row.find_all("td")
        name = re.sub(r"\s+", " ", columns[0].get_text().strip())
        name = re.sub(r"Date of Birth.*", "", name)
        country, address = parse_address(columns[1])
        nationality = columns[2].get_text().strip()
        start_date = parse_date(columns[3].get_text().strip())
        end_date = parse_date(columns[4].get_text().strip())
        details = columns[5].get_text().strip()
        name_lower = name.lower()
        if not name_lower in people:
            people[name_lower] = {"name": name, "legal_addresses": [], "nationalities": [], "specifications": []}
        people[name_lower]["specifications"].append({"start_date": start_date, "end_date": end_date, "details": details})
        people[name_lower]["legal_addresses"].append({"country": country, "street": address})
        people[name_lower]["nationalities"].append(nationality)
    return people


def parse_objects_from_second_table(url, people):
    table = get_second_table(url)
    rows = table.find_all("tr")[2:]
    for row in rows:
        columns = row.find_all("td")
        name = re.sub(r"\s+", " ", columns[0].get_text().strip())
        name = re.sub(r"Date of Birth.*", "", name)
        country, address = parse_address(columns[1])
        nationalities = [nat.replace("&", "and").replace("St", "Saint").strip() for nat in columns[2].get_text().strip().split("/")]
        start_date = parse_date(columns[3].get_text().strip())
        end_date = parse_date(columns[4].get_text().strip())
        pub_date = parse_date(columns[5].get_text().strip())
        details = f"{columns[6].get_text().strip()}\n{columns[7].get_text().strip()}"
        name_lower = name.lower()
        if not name_lower in people:
            people[name_lower] = {"name": name, "legal_addresses": [], "specifications": []}
        people[name_lower]["specifications"].append({"start_date": start_date, "end_date": end_date, "details": details, "pub_date": pub_date})
        people[name_lower]["legal_addresses"].append({"country": country, "street": address})
        people[name_lower]["nationalities"] = nationalities
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        for address in person_dictionary["legal_addresses"]:
            person.add_address(address["street"], '', '', '', address["country"], "LEGAL ADDRESS")
        for nationality in person_dictionary["nationalities"]:
            person.add_nationality(nationality)
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            specification.start_date = person_specification["start_date"]
            specification.end_date = person_specification["end_date"]
            if person_specification.get("pub_date"):
                specification.list_pub_date = person_specification["pub_date"]
            specification.add_rationale(person, "Ineligible Entities", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects_from_first_table(icle_config.link)
    people = parse_objects_from_second_table(icle_config.link, people)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25496"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
