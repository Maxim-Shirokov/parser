feature_20432: down build start_feature_20432

feature_9149: down build start_feature_9149

down:
	docker-compose down
	
build:
	docker-compose build

start_feature_9149:
	docker-compose run --volume=${PWD}/app/ app python feature_9149/main.py

start_feature_20432:
	docker-compose run --volume=${PWD}/app/ app python feature_20432/main.py
