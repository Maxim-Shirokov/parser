import os
import sys
import urllib.request, requests
import re

from icle import IcleSqlite, IcleConfig, IcleLogger
from icle.icle_sqlite.icle_sqlite import Validator

import fitz

from lxml import html


def get_year_urls(url):
    page = requests.get(url)
    tree = html.fromstring(page.content)
    xpath = "//section[@id='portlet_com_liferay_journal_content_web_portlet_JournalContentPortlet_INSTANCE_rwCkj056xB9R']//table//a"
    elements = tree.xpath(xpath)
    return ["https://www.organismocf.it" + element.attrib['href'] for element in elements]


def get_urls_download_file_with_details(url):
    year_urls = get_year_urls(url)
    urls = []
    for year_url in year_urls:
        page = requests.get(year_url)
        tree = html.fromstring(page.content)
        xpath = "//div[@id='collapseDeliberedivigilanza']//a[contains(@href,'.pdf')]"
        elements = tree.xpath(xpath)
        for element in elements:
            details = element.getparent().getnext().text
            if "sanzione" in details or "radiazione" in details or ("sospensione" in details and "un anno" in details):
                logger.log(logger.LogLevels.DEBUG, f"Found pdf file at url: {element.attrib['href']}")
                urls.append(("https://www.organismocf.it" + element.attrib["href"], details))
    return urls


def get_download_files(url):
    download_urls = get_urls_download_file_with_details(url)
    for download_url in download_urls:
        urllib.request.urlretrieve(download_url[0], f"{current_folder}/resources/file.pdf")
        yield download_url


def parse_objects(url):
    download_files_generator = get_download_files(url)
    name_regular = re.compile(r"(?:del\s+?sig\.|della\s+?sig\.ra)(?P<name>(?:[\w’]+|\s+)+?)\s{0,}\n", re.IGNORECASE)
    place_of_birth_regular = re.compile(r"nato a (?P<place_of_birth>[a-zA-Z]+)")
    date_of_birth_regular = re.compile(r"nato a.*?il.*?(?P<date_of_birth>\d{1,2} \w+ \d{4})")
    bad_pdf_urls = []
    good_pdf_files_count = 0
    for download_url in download_files_generator:
        download_url, details = download_url[0], download_url[1]
        logger.log(logger.LogLevels.INFO, f"Reading downloaded pdf file with url: {download_url}")
        pdf = fitz.open(f"{current_folder}/resources/file.pdf")
        pdf_text = ""
        for current_page in range(len(pdf)):
            page = pdf.loadPage(current_page)
            pdf_text += page.getText("text")

        name = name_regular.search(pdf_text)
        place_of_birth = place_of_birth_regular.search(pdf_text)
        date_of_birth = date_of_birth_regular.search(pdf_text)

        name = name.group("name").title().strip() if name else ""
        place_of_birth = place_of_birth.group("place_of_birth").strip() if place_of_birth else ""
        date_of_birth = date_of_birth.group("date_of_birth").strip() if date_of_birth else ""
        if name:
            good_pdf_files_count += 1
            yield {"name": name, "place_of_birth": place_of_birth, "date_of_birth": date_of_birth, "details": details}
        else:
            bad_pdf_urls.append(download_url)
        pdf.close()
    logger.log(logger.LogLevels.DEBUG, f"Found {good_pdf_files_count} good pdf files")
    logger.log(logger.LogLevels.DEBUG, f"Found {len(bad_pdf_urls)} files with bad encoding:'")
    logger.log(logger.LogLevels.DEBUG, bad_pdf_urls)


def parse_date(date):
    month_date = {
        "gennaio": "1",
        "febbraio": "2",
        "marzo": "3",
        "aprile": "4",
        "maggio": "5",
        "giugno": "6",
        "luglio": "7",
        "agosto": "8",
        "settembre": "9",
        "ottobre": "10",
        "novembre": "11",
        "dicembre": "12",
    }
    date = date.lower().split()
    date[1] = month_date[date[1]]
    return ".".join(date)


def create_objects(data_generator):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")

    for dictionary in data_generator:
        logger.log(logger.LogLevels.INFO, f"Saving element {str(dictionary)}")
        details = dictionary["details"]
        person = icle_sqlite.create_person()
        person.full_name = dictionary["name"]
        if dictionary["date_of_birth"]:
            date_of_birth = parse_date(dictionary["date_of_birth"])
            person.add_birth_date(date_of_birth)
        place_of_birth = dictionary["place_of_birth"]
        person.add_birth_place(city=place_of_birth, country_name="Italy", region="")
        specification = icle_sqlite.create_specification()
        specification.add_person(person)
        specification.add_rationale(person, "Supervisory resolutions", details)
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    data_generator = parse_objects(icle_config.link)
    create_objects(data_generator)
    print()


if __name__ == "__main__":
    DEBUG = False
    feature_code = "20432"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
