import os
import sys
import urllib.request, requests

from icle import IcleSqlite, IcleConfig, IcleLogger
from datetime import datetime

import tabula

from lxml import html


def get_url_download_file(url):
    page = requests.get(url)
    tree = html.fromstring(page.content)
    xpath = ".//a[contains(text(), 'Confidi minori cancellati dall’Elenco')]"
    element = tree.xpath(xpath)
    url = element[0].attrib['href'] if element else None
    return url


def get_download_file(url):
    download_url = get_url_download_file(url)
    urllib.request.urlretrieve(download_url, f"{current_folder}/resources/file.pdf")
    return download_url


def parse_date(date):
    month_date = {"gen": "1", "feb": "2", "mar": "3", "apr": "4", "mag": "5", "giu": "6" , "lug": "7", "ago": "8", "set": "9", "ott": "10", "nov": "11", "dic": "12"}
    date = date.lower().split("-")
    date[1] = month_date[date[1]]
    return datetime.strptime(".".join(date), "%d.%m.%y").strftime("%d.%m.%Y")


def parse_objects(url):
    download_url = get_download_file(url)
    logger.log(logger.LogLevels.INFO, f"Reading downloaded pdf file with url: {download_url}")
    pages = tabula.read_pdf(f"{current_folder}/resources/file.pdf", pages="all")
    people = {}
    for page in pages:
        rows_num = len(page)
        for row in range(rows_num):
            name = page.loc[row]["Denominazione"]
            fiscal_code = page.loc[row]["Codice fiscale"]
            street, city = page.loc[row]["Indirizzo Sede"].split(",")
            region = page.loc[row]["Regione"]
            zip = page.loc[row]["CAP"]
            start_date = parse_date(page.loc[row]["Data di\riscrizione"])
            end_date = parse_date(page.loc[row]["Data di\rcancellazione"])
            details = page.loc[row]["Natura del\rprovvedimento"] + "\n" + page.loc[row]["Forma\rgiuridica"]
            if name.lower() not in people:
                people[name.lower()] = {"name": name, "fiscal_codes": set(), "legal_addresses": [], "specifications": []}
            people[name.lower()]["fiscal_codes"].add(fiscal_code)
            people[name.lower()]["legal_addresses"].append({"street": street, "city": city, "region": region, "zip": zip})
            people[name.lower()]["specifications"].append({"start_date": start_date, "end_date": end_date, "details": details})
    return people

    
def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        for fiscal_code in person_dictionary["fiscal_codes"]:
            person.add_code(fiscal_code, "Fiscal code")
        for legal_address in person_dictionary["legal_addresses"]:
            person.add_address(legal_address["street"], legal_address["zip"], legal_address["city"], legal_address["region"], "Italy", "LEGAL ADDRESS")
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            specification.start_date = person_specification["start_date"] 
            specification.end_date = person_specification["end_date"]
            specification.add_rationale(person, "Deletion", person_specification["details"])  
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "21643"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")  
