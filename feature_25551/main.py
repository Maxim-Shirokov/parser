import os
import re
import sys
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger
from datetime import datetime

from bs4 import BeautifulSoup


details_regexp = re.compile(r"^<p><b>(?P<details>.*?)<\/b><\/p>$")
info_regexp = re.compile(r"(?:(?P<alias>.*?)\s—\s)?(?P<name>(?:.(?!Effective))+)(?P<start_date>.+)?")

common_details = "This is the U.S. Department of State’s list of entities and subentities under the control of, or acting for or on behalf of, the Cuban military, intelligence, or security services or personnel with which direct financial transactions would disproportionately benefit such services or personnel at the expense of the Cuban people or private enterprise in Cuba."


def parse_date(date):
    return datetime.strptime(date, "Effective %B %d, %Y").strftime("%d.%m.%Y")


def parse_objects(url):
    people = {}
    page = requests.get(url, verify=False)
    soup = BeautifulSoup(page.text, 'html.parser')
    div = soup.find("div", {"class": "entry-content"})
    elements = div.find_all("p")[2:-2]
    details = None
    for element in elements:
        possible_details = re.search(details_regexp, str(element))
        if possible_details:
            details = possible_details.group("details").replace(u'\xa0', u' ')
        else:
            info = re.search(info_regexp, element.get_text())
            if info:
                name = info.group("name").strip().replace(u'\xa0', u' ')
                alias = info.group("alias")
                start_date = parse_date(info.group("start_date").strip()) if info.group("start_date") else None
                name_lower = name.lower()
                if not name_lower in people:
                    people[name_lower] = {"name": name, "aliases": [], "specifications": []}
                if alias:
                    people[name_lower]["aliases"].append(alias.strip())
                people[name_lower]["specifications"].append(
                    {"start_date": start_date, "details": details + "\n" + common_details}
                )
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        if person_dictionary["aliases"]:
            person.alias = "; ".join(person_dictionary["aliases"])
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            if person_specification["start_date"]:
                specification.start_date = person_specification["start_date"]
            specification.add_rationale(person, "Cuba Restricted List", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25551"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
