import os, json

class Validator():
    
    def __init__(self):
        parameters_directory_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        parameters_file_name = 'utilities/fixed_fields.json'
        synonyms_file_name = 'utilities/synonyms.json'
        nationalities_file_name = 'utilities/nationalities.json'
        parameters_file_path = os.path.join(parameters_directory_path, parameters_file_name)
        synonyms_file_path = os.path.join(parameters_directory_path, synonyms_file_name)
        nationalities_file_path  = os.path.join(parameters_directory_path, nationalities_file_name)
        acceptable_fields_json = json.load(open(parameters_file_path, encoding="utf-8") )
        self.__acceptable_codes = acceptable_fields_json["code_types"]
        self.__acceptable_addresses = acceptable_fields_json["address_types"]
        self.__acceptable_countries = acceptable_fields_json["countries"]
        self.__country_synonyms = self.__dict_to_upper(json.load(open(synonyms_file_path, encoding="utf-8") ))
        self.__nationalities = self.__dict_to_upper(json.load(open(nationalities_file_path, encoding="utf-8") ))
    
    def __dict_to_upper(self, d):
        r = {}
        for k, v in d.items():
            K = k.upper()
            r[K] = v if not r.__contains__(K) else v + r[K]
        return r
        
    def is_country_valid(self, country_name):
        return country_name.upper() in self.__acceptable_countries
    
    def is_address_type_valid(self, address_type):
        return address_type.upper() in self.__acceptable_addresses
    
    def is_nationality_valid(self, nationality):
        return nationality.strip().upper() in list(list(self._Validator__nationalities.values()))
    
    def is_code_type_valid(self, code_type):
        return code_type.upper() in self.__acceptable_codes
    
    def get_actual_country_name(self, raw_name):
        if self.is_country_valid(raw_name):
            return raw_name.title()
        else:
            if raw_name.upper() in self.__country_synonyms:
                return self.__country_synonyms[raw_name.upper()]
            else:
                return None
    
    def get_actual_nationality(self, raw_name):
        if self.is_nationality_valid(raw_name):
            return raw_name.title()
        else:
            actual_country = self.get_actual_country_name(raw_name)
            if actual_country is None:
                return None
            if actual_country.upper() in self.__nationalities:
                return self.__nationalities[actual_country.upper()].title()
        return None
        