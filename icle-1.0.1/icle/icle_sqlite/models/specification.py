from enum import Enum
from datetime import datetime
import sys, calendar, os, base64, requests
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error
from models.person_derived import PersonDerived #pylint: disable=import-error
from models.list import List #pylint: disable=import-error
from models.link import Link #pylint: disable=import-error
from models.source import Source #pylint: disable=import-error
from models.authority import Authority #pylint: disable=import-error
from models.person import Person #pylint: disable=import-error
from models.country import Country #pylint: disable=import-error
from models.rationale import Rationale, RationaleType #pylint: disable=import-error

class Category(DbModel):
    
    class DbFields(Enum):
        name = 'name'
    
    def __init__(self):
        super().__init__()
        self.name = None
    
    def save(self, db):
        command = self.insert_command("category", 
                [self.DbFields.name.value],
                [self.name])
        super().save(db, command)
        
        
class Subcategory(DbModel):
    
    class DbFields(Enum):
        name = 'function_name'
        category_id = "category_id"

    def __init__(self, categories=[]):
        super().__init__()
        self.name = None
        self.category = None
    
    def save(self, db):
        command = self.insert_command("subcategory", 
                [self.DbFields.name.value, self.DbFields.category_id.value],
                [self.name, self.category.id])
        super().save(db, command)
        

class Specification(DbModel):
    
    class DbFields(Enum):
        list_publ_date = "list_publication_date"
        list_update = "list_update"
        date_start = "date_start"
        date_end = "date_end"
        subcategory_id = "subcategory_id"
        doc_pdf = "doc_pdf"
        import_code = "import_code"
        list_id = "list_id"
        source_table_id = "source_id"
        pdf_file = "pdf_file"
    
    def __init__(self, feature_code, default_link_value=None, default_list=None, default_source=None, default_authority=None, default_subcategory=None, get_rationale_types=None):
        super().__init__()
        self.__list_pub_date = None
        self.list_update = datetime.now().strftime('%d.%m.%Y')
        self.__date_start = None
        self.__date_end = None
        self.url_doc_pdf = None
        self.doc_pdf_data = None
        self.import_code = "#{}-{}".format(feature_code, datetime.now().strftime("%Y.%m.%d"))
        self.__source = default_source
        self.__subcategory = default_subcategory
        self.__list = default_list
        self.__subcategory = default_subcategory
        self.__persons = []
        self.__rationales = []
        self.__get_rationale_types = get_rationale_types
        self.__links = [] if not default_link_value else [Link(link=default_link_value, specification=self)]
    
    def __parse_date(self, value, set_start=True):
        formats = ["%d.%m.%Y", "%m.%Y", "%Y"]
        for index, _format in enumerate(formats):
            try:
                date_val = datetime.strptime(value, _format)
                if index == 2 and not set_start:
                    date_val = date_val.replace(month = 12)
                if index > 0 and not set_start:
                    _, num_days = calendar.monthrange(date_val.year, date_val.month)
                    date_val = date_val.replace(day = num_days)
                return date_val
            except Exception as _:
                continue
        return None
    
    @property
    def list_pub_date(self):
        return self.__list_pub_date
    @list_pub_date.setter
    def list_pub_date(self, value):
        if value is None or value == "":
            self.__list_pub_date = None
            return
        date_val = self.__parse_date(value)
        if date_val is None:
            raise Exception("Invalid date format")
        
        if date_val > datetime.now():
            raise Exception("Publication date cannot be in the future")
        self.__list_pub_date = value
    
    @property
    def date_start(self):
        return self.__date_start
    @date_start.setter
    def date_start(self, value):
        if value is None or value == "":
            self.__date_start = None
            return
        
        date_val = self.__parse_date(value)
        if date_val is None:
            raise Exception("Invalid date format")
        if self.date_end is not None:
            date_end_val = self.__parse_date(self.date_end)
            if date_val > date_end_val:
                raise Exception("Date start cannot be greater than date end")
        self.__date_start = value
        
    @property
    def date_end(self):
        return self.__date_end
    @date_end.setter
    def date_end(self, value):
        if value is None or value == "":
            self.__date_end = None
            return
        
        date_val = self.__parse_date(value, set_start=False)
        if date_val is None:
            raise Exception("Invalid date format")
        if self.date_start is not None:
            date_start_val = self.__parse_date(self.date_start)
            if date_start_val > date_val:
                raise Exception("Date start cannot be greater than date end")
        self.__date_end = value
        
        
    def set_and_download_url_doc(self, url):
        if url is None or url == "":
            raise Exception("URL must be specified")
        self.url_doc_pdf = url
        self.doc_pdf_data = base64.b64encode(requests.get(url).content).decode('utf-8')
        
    def set_list(self, _list):
        if _list is None:
            raise Exception('List must be specified')
        if type(_list) != List:
            raise Exception('List is not the expected type')
        self.__list = _list

    def set_source(self, source):
        if source is None:
            raise Exception('Source must be specified')
        if type(source) != Source:
            raise Exception('Source is not the expected type')
        self.__source = source
    
    def set_subcategory(self, subcategory):
        if subcategory is None:
            raise Exception('Subcategory must be specified')
        if type(subcategory) != Subcategory:
            raise Exception('Subcategory is not the expected type')
        self.__subcategory = subcategory
    
    def add_person(self, person):
        if not person:
            raise Exception('Person must be specified')
        if type(person) != Person:
            raise Exception('Person is not the expected type')
        person._Person__get_spec_hashing_variables = lambda: (self.__list.name if self.__list else None, self.__list.authority.name if self.__list else None)
        person_specification = SpecificationPerson()
        person_specification.person = person
        person_specification.specification = self
        for ps_added in self.__persons:
            if ps_added.person == person:
                return
        self.__persons.append(person_specification)
    
    def add_link(self, link_value, unmonitorable=False):
        if not link_value or link_value == "":
            raise Exception('Link must be specified')
        for link_added in self.__links:
            if link_added.link == link_value:
                return
        link = Link(link_value, unmonitorable=unmonitorable)
        link.specification = self
        self.__links.append(link)
        
    def add_rationale(self, person, rationale, details):
        if not person:
            raise Exception('Person must be specified')
        if person not in [x.person for x in self.__persons]:
            raise Exception('Person needs to be added to the specifications before adding a rationale')
        if type(person) != Person:
            raise Exception('Person is not the expected type')
        
        if not rationale:
            raise Exception('Rationale must be specified')
        
        details = details.strip().strip(",").strip().replace("  ", " ")
        rationale = rationale.strip().strip(",").strip().replace("  ", " ")
        
        _rationale = Rationale()
        _rationale.value = details.strip().strip(",").strip().replace("  ", " ")
        rationale_match = [rat for rat in self.__get_rationale_types() if rat.value == rationale]
        if any(rationale_match):
            rationale_type = rationale_match[0] 
        else:
            rationale_type = RationaleType()
            rationale_type.value = rationale
            self.__get_rationale_types().append(rationale_type)
            
        _rationale.rationale_type = rationale_type
        _rationale.person_specification = next(iter([pers_spec for pers_spec in self.__persons if pers_spec.person==person]))
        if not _rationale.person_specification:
            raise Exception('Person must be added to the specification before assigning a rationale')
        self.__rationales.append(_rationale)
    
    def save(self, db):
        if len(self.__links) < 1:
            raise Exception('Specifications need to have at least one link assigned')
        if self.id is None:
            command = self.insert_command("specification", 
                    [Specification.DbFields.list_publ_date.value, Specification.DbFields.list_update.value, Specification.DbFields.date_start.value,
                    Specification.DbFields.date_end.value, Specification.DbFields.subcategory_id.value, Specification.DbFields.doc_pdf.value,
                    Specification.DbFields.list_id.value, Specification.DbFields.source_table_id.value, Specification.DbFields.import_code.value,
                    Specification.DbFields.pdf_file.value],
                    [self.list_pub_date, self.list_update, self.date_start, self.date_end, self.__subcategory.id, self.url_doc_pdf,
                    self.__list.id, self.__source.id, self.import_code, self.doc_pdf_data])
            super().save(db, command)
                
        for person_spec in self.__persons:
            person_spec.save(db)
        
        for rationale in self.__rationales:
            rationale.save(db)
        
        for link in self.__links:
            link.save(db)
        
        
        
class SpecificationPerson(DbModel):
    
    class DbFields(Enum):
        specification_id = "specification_id"
        person_id = "person_id"
    
    def __init__(self):
        super().__init__()
        self.specification = None
        self.person = None

    def save(self, db):
        if not self.person or self.person.id is None:
            pass
        command = self.insert_command("person_specification", 
                [SpecificationPerson.DbFields.person_id.value, SpecificationPerson.DbFields.specification_id.value],
                [self.person.id, self.specification.id])
        super().save(db, command)