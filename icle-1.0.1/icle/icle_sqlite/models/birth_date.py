import sys, os, pandas, datetime
from enum import Enum
sys.path.append(os.path.realpath(os.path.dirname(__file__)))

from models.person_derived import PersonDerived #pylint: disable=import-error

class BirthDate(PersonDerived):
    
    class DbFields(Enum):
        day = "day"
        month = "month"
        year = "year"
        date_assumed = "int_date_assumed"
        person_id = "person_id"
    
    def __init__(self):
        super().__init__()
        self.full_date_string = None
        self.date_assumed = None
        self.day = None
        self.month = None
        self.year = None
    
    @property
    def to_date(self):
        return datetime.datetime(self.year, self.month if self.month else 1, self.day if self.day else 1)
    
    @property
    def to_string(self, format="%d.%m.%Y"):
        return self.to_date.strftime(format)
    
    def save(self, db):
        command = self.insert_command("birth_date", 
                                [self.DbFields.day.value, self.DbFields.month.value, self.DbFields.year.value, 
                                    self.DbFields.date_assumed.value, self.DbFields.person_id.value], 
                                [self.day, self.month, self.year, self.date_assumed, self.person.id])
        super().save(db, command)