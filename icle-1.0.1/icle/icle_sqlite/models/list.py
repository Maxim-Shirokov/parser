from enum import Enum
import sys, os
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error

class List(DbModel):
    
    class DbFields(Enum):
        name = "name"
        authority_id = "authority_id"
    
    def __init__(self):
        super().__init__()
        self.name = None
        self.authority = None
        #self.db = db_connector
    
    def save(self, db):
        command = self.insert_command("list", 
                                [List.DbFields.name.value, List.DbFields.authority_id.value],
                                [self.name, self.authority.id])
        super().save(db, command)
        