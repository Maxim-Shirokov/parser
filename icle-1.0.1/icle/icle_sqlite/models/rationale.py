import sys, os, pandas
from enum import Enum
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.person_derived import PersonDerived #pylint: disable=import-error
from models.database_model import DbModel #pylint: disable=import-error


class RationaleType(DbModel):
    
    class DbFields(Enum):
        id = "id"
        name = "rationale_name"
    
    def save(self, db):
        command = self.insert_command("rationale_type",
                [self.DbFields.name.value], 
                [self.value])
        super().save(db, command)
        
    def __init__(self):
        super().__init__()
        self.value = ""
            
  
class Rationale(DbModel):
    
    class DbFields(Enum):
        value = "details"
        person_specification_id = "person_spec_id"
        rationale_type_id = "rationale_type_id"
    
    def save(self, db):
        self.rationale_type.save(db)
        command = self.insert_command("rationale",
                [self.DbFields.value.value, 
                self.DbFields.person_specification_id.value, 
                self.DbFields.rationale_type_id.value,], 
                [self.value, self.person_specification.id, self.rationale_type.id])
        super().save(db, command)
    
    def __init__(self):
        super().__init__()
        self.value = ""
        self.rationale_type = None
        self.person_specification = None