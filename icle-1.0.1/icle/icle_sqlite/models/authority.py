from enum import Enum
import pandas, sys, os
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error
from models.country import Country #pylint: disable=import-error

class Authority(DbModel):
    
    class DbFields(Enum):
        id = "id"
        name = "authority_name"
        acronym = "authority_acronym"
        country_id = "country_id"
    
    def __init__(self):
        super().__init__()
        self.name = None
        self.acronym = None
        self.country = None    
     
    def save(self, db):
        command = self.insert_command("authority",
                [self.DbFields.name.value, self.DbFields.acronym.value, 
                self.DbFields.country_id.value], 
                [self.name, self.acronym, self.country.id if self.country else None])
        super().save(db, command)