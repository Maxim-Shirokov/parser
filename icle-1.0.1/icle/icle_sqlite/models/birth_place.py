import sys, os
import pandas
from enum import Enum
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.person_derived import PersonDerived #pylint: disable=import-error
from models.country import Country #pylint: disable=import-error

class BirthPlace(PersonDerived):
    
    class DbFields(Enum):
        country = "country_id"
        region = "region"
        city = "city"
        person_id = "person_id"
        
    def save(self, db):
        command = self.insert_command("birth_place", 
                                [self.DbFields.person_id.value, self.DbFields.country.value, self.DbFields.region.value, self.DbFields.city.value], 
                                [self.person.id, self.country.id, self.region, self.city])
        super().save(db, command)
            
    def __init__(self, file=None):
        super().__init__()
        
        self.country = None
        self.region = None
        self.city = None
    