from typing import final
import numpy, pandas, datetime, sys, os, json, base64, requests
import hashlib, re
from datetime import datetime
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error
from models.birth_date import BirthDate #pylint: disable=import-error
from models.birth_place import BirthPlace #pylint: disable=import-error
from models.address import Address, AddressType #pylint: disable=import-error
from models.code import Code, CodeType #pylint: disable=import-error
from models.nationality import Nationality, NationalityPerson #pylint: disable=import-error
from models.country import Country #pylint: disable=import-error
sys.path.append(os.path.realpath('../'))
from utilities.validator import Validator #pylint: disable=import-error
from enum import Enum

class Person(DbModel):
    already_created_identifiers = []
    
    class DbFields(Enum):
        title = "title"
        gender = "gender"
        first_name = "first_name"
        last_name = "last_name"
        full_name = "full_name"
        alias = "alias"
        native_script = "native_script"
        entity_type_id = "entity_type_id"
        pep_id = "pep_id"
        image = "image"
        image_url = "image_url"
        details = "details"
    
    class IdentifierDbFields(Enum):
        person_id = "person_id"
        value = "value"
    
    def __init__(self, validator=None, auto_capitalization=True, get_countries=[], get_nationalities=[], get_address_types=[], get_code_types=[]):
        super().__init__()
        self.title = None
        self.__gender = None
        self.__first_name = None
        self.__last_name = None
        self.__full_name = None
        self.alias = None
        self.__native_script = None
        self.opu_id = None
        self.__entity_type = None
        self.today = None
        self.url_image = None
        self.image_data = None
        self.pep_id = None
        self.details = None
        self.auto_capitalization = auto_capitalization
        self.__codes = []
        self.__birth_dates = []
        self.__birth_places = []
        self.__addresses = []
        self.__nationalities = []
        self.__identifiers = []
        self.__get_countries = get_countries
        self.__get_nationalities = get_nationalities
        self.__get_address_types = get_address_types
        self.__get_code_types = get_code_types
        self.__validator = validator if validator is not None else Validator()
        def __get_spec_hashing_variables(self, x): return None, None
    
    def __clean_name(self, raw_name):
        raw_name = raw_name.strip().strip(",").strip(".")
        raw_name = raw_name.replace("  ", " ").strip()
        unallowed_chars = ["(", ")", "\"", "[", "]", "{", "}"]
        for char in unallowed_chars:
            raw_name.replace(char, "")
        final_name = raw_name.replace(" S.P.A", " S.p.A").replace(" S.R.L", " S.r.l").replace(" Llc", " LLC") if not self.__is_website(raw_name) else raw_name.lower()
        return final_name.title() if self.auto_capitalization else final_name
    
    @property
    def first_name(self):
        return self.__first_name
    @first_name.setter
    def first_name(self, value):
        value = self.__clean_name(value)
        if value is None or value == "":
            self.__first_name = None
            return
        if self.entity_type != "P" and self.entity_type is not None:
            raise Exception("First name can only be specified to persons")
        if any([x for x in value if not x.isalpha() and x not in [" ", "'", "-", ".", "’"]]):
            raise Exception("First name can only contain characters")
        if value.endswith("."):
            raise Exception("First name ends with a dot")
        self.__first_name = value
        self.entity_type = "P"
    
    @property
    def last_name(self):
        return self.__last_name
    
    @last_name.setter
    def last_name(self, value):
        value = self.__clean_name(value)
        if value is None or value == "":
            self.__last_name = None
            return
        if self.entity_type != "P" and self.entity_type is not None:
            raise Exception("Last name can only be specified to persons")
        if any([x for x in value if not x.isalpha() and x not in [" ", "'", "-", ".", "’", "‘"]]):
            raise Exception("Last name can only contain characters")
        if value.endswith("."):
            raise Exception("Last name ends with a dot")
        self.__last_name = value
        self.entity_type = "P"
    
    @property
    def full_name(self):
        return self.__full_name
    
    @full_name.setter
    def full_name(self, value):    
        value = self.__clean_name(value)
        
        if value is None or value == "":
            self.__full_name = None
            return
        if self.first_name is not None:
            raise Exception("Cannot set both first_name and full_name")
        if self.last_name is not None:
            raise Exception("Cannot set both last_name and full_name")
        self.__full_name = value
        if self.__is_website(self.__full_name):
            self.__full_name = self.__full_name.lower()
            self.entity_type = "W"
    
    def __is_website(self, text):
        regex = r"^(?:www.|http:|https:)?[a-z0-9\-\\\/\.]*\.(?:com|it|co|io|uk|org)/?$"
        if re.fullmatch(regex, text.lower()) is not None:
            return True
        return False
    
    @property
    def gender(self):
        return self.__gender
    
    @gender.setter
    def gender(self, value):
        value = value.strip()
        if value.upper() not in ["NA", "F", "M"]:
            raise Exception("Invalid gender")
        self.__gender = value.upper()
        if self.__gender != "NA":
            self.entity_type = "P"
    
    @property
    def entity_type(self):
        return self.__entity_type
    
    @entity_type.setter
    def entity_type(self, value):
        value = value.strip()
        if value.upper() not in ["NA", "P", "L", "W"]:
            raise Exception("Invalid entity type")
        self.__entity_type = value.upper()
        if self.__entity_type != "P":
            self.gender = "NA"
    
    @property
    def native_script(self):
        return self.__native_script
    
    @native_script.setter
    def native_script(self, value):
        #TODO Some languages contain roman characters even though they also use non roman letters 
        #if re.search('[a-zA-Z]', value) is not None:
        #    raise Exception("Native script must not contain roman characters")
        self.__native_script = value
    
    def set_and_download_url_image(self, url):
        if url is None or url == "":
            raise Exception("URL must be specified")
        self.url_image = url
        self.image_data = base64.b64encode(requests.get(url, verify=False).content).decode('utf-8')
        
    def __get_entity_type_id(self):
        values_id = {
            "NA": 1,
            "P": 2,
            "L": 3,
            "W": 4
        }
        if self.entity_type not in values_id:
            return 1
        return values_id[self.entity_type]
    
    def __create_hash_identifier(self): 
        list_name, authority_name = self.__get_spec_hashing_variables()  
        values = [self.first_name, self.last_name, self.full_name, list_name, authority_name, self.entity_type if self.entity_type is not None else "NA"] + [x.to_string for x in self.__birth_dates]
        unhashed_value = "-".join(list(map(lambda x: str(x) if x is not None else "$", values)))
        hash_object = hashlib.md5(unhashed_value.encode())
        return hash_object.hexdigest()
       
    def save(self, db):
        if not self.first_name and not self.last_name and not self.full_name:
            raise Exception("Cannot add a person without first name, last name and full name")
        command = self.insert_command("person", 
                [self.DbFields.title.value, self.DbFields.gender.value, self.DbFields.first_name.value, self.DbFields.last_name.value, self.DbFields.full_name.value,
                self.DbFields.alias.value, self.DbFields.native_script.value, self.DbFields.entity_type_id.value,  self.DbFields.image.value, self.DbFields.image_url.value, self.DbFields.details.value],
                [self.title, self.gender if self.gender else "NA", self.first_name, self.last_name, self.full_name, 
                self.alias, self.native_script, self.__get_entity_type_id(), 
                self.image_data, self.url_image, self.details])
        super().save(db, command)
        
        self.save_identifiers(db)
        for _address in self.__addresses:
            _address.save(db)
        for _code in self.__codes:
            _code.save(db)
        for _birthdate in self.__birth_dates:
            _birthdate.save(db)
        for _birthplace in self.__birth_places:
            _birthplace.save(db)
        for _nationality in self.__nationalities:
            _nationality.save(db)
     
    def save_identifiers(self, db):
        self.__identifiers.append(self.__create_hash_identifier())
        if self.image_data:
            hash_object = hashlib.md5(self.image_data.encode())
            self.__identifiers.append(f"IMG:{hash_object.hexdigest()}")
        
        for _identifier in self.__identifiers:
            command = self.insert_command("identifiers", 
                [self.IdentifierDbFields.person_id.value, self.IdentifierDbFields.value.value],
                [self.id, _identifier])
            super().save(db, command, False)
    
    def __isBlank (self, myString):
        return not (myString and myString.strip())
    
    def add_address(self, street, zip, city, region, country, address_type):
        if self.__isBlank(street) and self.__isBlank(city) and self.__isBlank(region) and self.__isBlank(country):
            raise Exception("At least one of the parameters must be specified must be specified")
        if self.__isBlank(address_type):
            raise Exception("Address type must be specified")
        if not self.__validator.is_address_type_valid(address_type): #if address_type.upper() not in self.__acceptable_addresses:
            raise Exception("Address type {} is not acceptable".format(address_type))
        country_parsed = self.__validator.get_actual_country_name(country)
        if country_parsed is None:
            raise Exception("Country {} is not acceptable".format(country))
        address = Address()
        address.street = street
        address.city = city
        address.zip = zip
        address.region = region
        address.country = country_parsed
        address_type_match = [adt for adt in self.__get_address_types() if adt.value == address_type]
        if any(address_type_match):
            adt = address_type_match[0] 
        else:
            adt = AddressType()
            adt.value = address_type
            self.__get_address_types().append(adt)
        address.address_type = adt
        address.person = self
        for address_added in self.__addresses:
            if address.address_type == address_added.address_type and address_added.street == address.street and address_added.city == address.city and address_added.region == address.region and address_added.country == address.country:
                return
        self.__addresses.append(address)
        return address
    
    def add_identifier(self, identifier):
        if identifier in Person.already_created_identifiers and identifier not in self.__identifiers:
            raise Exception("This identifier was already added to another person!")
        if identifier not in self.__identifiers:
            self.__identifiers.append(identifier)
            Person.already_created_identifiers.append(identifier)
        
    def add_birth_place(self, city, region, country_name):
        if self.__isBlank(city) and self.__isBlank(region) and self.__isBlank(country_name):
            raise Exception("At least one of the parameters must be specified must be specified")
        country_parsed = self.__validator.get_actual_country_name(country_name)
        if country_parsed is None:
            raise Exception("Country {} is not acceptable".format(country_name))
        birth_place = BirthPlace()
        birth_place.city = city
        birth_place.region = region
        country_match = [ctry for ctry in self.__get_countries() if ctry.name == country_parsed]
        if any(country_match):
            ctr = country_match[0] 
        else:
            ctr = Country()
            ctr.name = country_parsed
            self.__get_countries().append(ctr)
        birth_place.country = ctr
        birth_place.person = self
        for birth_place_added in self.__birth_places:
            if birth_place_added.city == birth_place.city and birth_place_added.region == birth_place.region and birth_place_added.country == birth_place.country:
                return
        self.__birth_places.append(birth_place)
        self.entity_type = "P"
        
    def add_birth_date(self, date, format="%d.%m.%Y", date_assumed = False):
        if self.__isBlank(date):
            raise Exception("Date must be specified")
        date_value = datetime.strptime(date, format) if type(date) is str else date
        birth_date = BirthDate()
        birth_date.day = date_value.day if any(x for x in ["%d", "%c", "%x", "%j"] if x in format) else None
        birth_date.month = date_value.month if any(x for x in ["%B", "%m", "%b", "%c", "%x", "%j"] if x in format) else None
        birth_date.year = date_value.year
        birth_date.date_assumed = 1 if date_assumed else 0 
        birth_date.person = self
        for birth_date_added in self.__birth_dates:
            if birth_date_added.day == birth_date.day and birth_date_added.month == birth_date.month and birth_date_added.year == birth_date.year and birth_date_added.date_assumed == birth_date.date_assumed:
                return
        self.__birth_dates.append(birth_date)
    
    def add_code(self, code, code_type):
        if self.__isBlank(code_type):
            raise Exception("Code type must be specified")
        if self.__isBlank(code):
            raise Exception("Code must be specified")
        if not self.__validator.is_code_type_valid(code_type):
            raise Exception("Code type is not acceptable")
        _code = Code()
        _code.value = code
        code_match = [cty for cty in self.__get_code_types() if cty.name == code_type]
        if any(code_match):
            cty = code_match[0] 
        else:
            cty = CodeType()
            cty.name = code_type
            self.__get_code_types().append(cty)
        _code.code_type = cty
        _code.person = self
        for code_added in self.__codes:
            if code_added.value == _code.value and code_added.code_type == _code.code_type:
                return
        self.__codes.append(_code)
    
    def add_nationality(self, nationality):
        if self.__isBlank(nationality):
            raise Exception("Nationality must be specified")
        if not self.__validator.is_nationality_valid(nationality):
            nationality_result = self.__validator.get_actual_nationality(nationality)
            if nationality_result is None:
                raise Exception("Nationality {} is not acceptable".format(nationality))
        else:
            nationality_result = nationality.title()
        _nationality = NationalityPerson()
        nationality_match = [nat for nat in self.__get_nationalities() if nat.name == nationality_result]
        if any(nationality_match):
            nat = nationality_match[0] 
        else:
            nat = Nationality()
            nat.name = nationality_result
            self.__get_nationalities().append(nat)
        _nationality.nationality = nat
        _nationality.person = self
        for nationality_added in self.__nationalities:
            if nationality_added.nationality == _nationality.nationality:
                return
        self.__nationalities.append(_nationality)