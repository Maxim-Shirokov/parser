import sys, os
import pandas
from enum import Enum
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error
from models.person_derived import PersonDerived #pylint: disable=import-error

class AddressType(DbModel):
    
    class DbFields(Enum):
        name = "type_name"
    
    def __init__(self):
        super().__init__()
        self.value = None
    
    def save(self, db):
        command = self.insert_command("address_type", 
                [self.DbFields.name.value],
                [self.value])
        super().save(db, command)
    
class Address(PersonDerived):
    
    class DbFields(Enum):
        country = "country"
        country_iso = "country_iso"
        region = "region"
        city = "city"
        street = "street"
        zip = "zip"
        person_id = "person_id"
        address_type_id = "address_type_id"
    
    def save(self, db):
        self.address_type.save(db)
        command = self.insert_command("address", 
                [self.DbFields.country.value, self.DbFields.country_iso.value, self.DbFields.region.value, self.DbFields.city.value, self.DbFields.street.value,
                self.DbFields.zip.value, self.DbFields.address_type_id.value, self.DbFields.person_id.value],
                [self.country, self.country_iso, self.region, self.city, self.street, self.zip, self.address_type.id, self.person.id])
        super().save(db, command)
        
    def __init__(self):
        super().__init__()
        self.country = None
        self.country_iso = None
        self.region = None
        self.city = None
        self.street = None
        self.zip = None
        self.address_type = None