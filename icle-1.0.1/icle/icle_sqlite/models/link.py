from enum import Enum
import pandas, sys, os
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error

class Link(DbModel):
    
    class DbFields(Enum):
        link = "link"
        unmonitorable = "unmonitorable"
        description = "description"
        public = "public"
        specification_id = "specification_id"
    
    def __init__(self, link=None, unmonitorable=False, specification=None):
        super().__init__()
        self.link = link
        self.unmonitorable = 1 if unmonitorable else 0
        self.description = ""
        self.public = 1
        self.specification = specification
    
    def save(self, db):
        command = self.insert_command("link", 
                                [Link.DbFields.link.value, Link.DbFields.unmonitorable.value, Link.DbFields.description.value, Link.DbFields.public.value, Link.DbFields.specification_id.value],
                                [self.link, self.unmonitorable, self.description, self.public, self.specification.id])
        super().save(db, command)