import json

class DbModel():
    
    
    def bind_to(self, observable):
        self._observables.append(observable)
    
    def __init__(self):
        self._observables = []
        self.__id = None
    
    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, value):
        self.__id = value
        for observable in self._observables:
            observable(self.__id)
    
    def insert_command(self, table_name, columns, values):
        command = "INSERT INTO {}".format(table_name)
        command += " (" + ",".join(columns) + ") values ("
        for index, element in enumerate(values):
            if index > 0:
                command += ", "
            if element is not None and element != "":
                if callable(element):
                    command += str(element())
                else:
                    command += "'" + str(element).replace("'", "''") + "'"
            else:
                command += "NULL"
        
        command += " )"
        return command
    
    def save(self, db, command, set_id=True):
        if self.id is None and set_id:
            self.id = db.execute_command(command) 
        if not set_id:
            db.execute_command(command) 