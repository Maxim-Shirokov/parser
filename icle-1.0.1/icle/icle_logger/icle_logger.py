import logging, os, sys
import pandas
from traceback import format_exception
from datetime import datetime

class IcleLogger():
    
    class LogLevels():
        FATAL = 50000
        ERROR = 40000
        WARN = 30000
        INFO = 20000
        DEBUG = 10000
    
    class LogColumns():
        Level = "Level"
        Message = "Message"
        Datetime = "Datetime"
    
    def __init__(self, feature_code, directory="./logs", separate_folders_by_day=True):
        if separate_folders_by_day:
            self.directory_name = datetime.today().strftime("%d.%m.%Y")
            if not os.path.exists(os.path.join(directory, self.directory_name)):
                os.makedirs(os.path.join(directory, self.directory_name))
            directory = os.path.join(directory, self.directory_name)
        filename = feature_code + "-" + datetime.today().strftime("%Y.%m.%d_%H%M") + ".log"
        self.__path = os.path.join(directory, filename)
        self.__log_dict = []
        sys.excepthook = self.__handle_exception
             
    def __handle_exception(self, exc_type, exc_value, exc_traceback):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        f_exc = format_exception(exc_type, exc_value, exc_traceback)
        self.log(IcleLogger.LogLevels.FATAL, "".join(f_exc).replace("\"", "\"\""))
    
    def log(self, level, message):
        with open(self.__path, 'a', encoding='utf-8') as log_file:
            if os.path.getsize(self.__path) == 0:
                log_file.write('"{}","{}", "{}"\n'.format(IcleLogger.LogColumns.Level, IcleLogger.LogColumns.Message, IcleLogger.LogColumns.Datetime))    
            log_file.write('{},"{}", "{}"\n'.format(level, message, datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")))
