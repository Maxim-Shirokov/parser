from datetime import datetime
import os
import re
import sys
import urllib.request, requests

from icle import IcleSqlite, IcleConfig, IcleLogger

from bs4 import BeautifulSoup

import xml.etree.ElementTree as ET


base_url = "https://www.publicsafety.gc.ca/"
xml_url = "{http://www.w3.org/2005/Atom}"
alias_regular = re.compile(r"(?P<alias>\(.*?\))")


def parse_date(date):
    return datetime.strptime(date, '%Y-%m-%d').strftime("%d.%m.%Y")


def get_xml(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    tag_a = soup.find("a", href=lambda href: href and ".xml" in href)
    return base_url + tag_a.get('href')


def get_download_file(url):
    download_url = get_xml(url)
    urllib.request.urlretrieve(download_url, f"{current_folder}/resources/file.xml")
    return download_url


def parse_objects(url):
    download_url = get_download_file(url)
    logger.log(logger.LogLevels.INFO, f"Reading downloaded pdf file with url: {download_url}")
    people = {}
    tree = ET.parse(f"{current_folder}/resources/file.xml")
    root = tree.getroot()
    for child in root.iter(f"{xml_url}entry"):
        name = child.find(f"{xml_url}title").text
        alias_in_brackets = re.search(alias_regular, name)
        alias_in_brackets = alias_in_brackets.group("alias") if alias_in_brackets else ''
        pub_date = child.find(f"{xml_url}published").text
        pub_date = parse_date(pub_date)
        content = child.find(f"{xml_url}content").text
        updated = child.find(f"{xml_url}updated").text
        details = f"{content}\n{updated}"
        alias = child.find(f"{xml_url}summary").text
        if alias_in_brackets:
            alias += f"; {alias_in_brackets}"
        name_lower = name.lower()
        if name_lower not in people:
            people[name_lower] = {"name": name, "specifications": []}
        if alias:
            people[name_lower]["alias"] = alias
        people[name_lower]["specifications"].append({"details": details, "pub_date": pub_date})
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        person.alias = person_dictionary["alias"]
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.list_pub_date = person_specification["pub_date"]
            specification.add_person(person)
            specification.add_rationale(person, "Listed Entities", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25505"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
