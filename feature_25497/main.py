import os
import re
import sys
import requests

from icle import IcleSqlite, IcleConfig, IcleLogger
from datetime import datetime

from bs4 import BeautifulSoup


base_url = "https://www.ebrd.com"


def parse_date(date):
    date = date.replace("-", " ").replace("Ongoing", "").replace("Indefinite", "").replace("Permanent", "").strip()
    return datetime.strptime(date, "%d %b %Y").strftime("%d.%m.%Y") if date else None


def parse_addresses(addresses):
    p_tags = addresses.find_all("p")
    if not p_tags:
        return [re.sub(r"\s+", " ", addresses.get_text().strip().replace("\xa0", " "))]
    return [re.sub(r"\s+", " ", p_tag.get_text().strip().replace("\xa0", " ")) for p_tag in p_tags]


def get_pages_url(url):
    pages_url = [url]
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    tags_a = soup.find("div", {"class": "saf-paging"}).find_all("a")
    for tag_a in tags_a:
        pages_url.append(f"{base_url}{tag_a.get('href')}")
    yield from pages_url


def parse_objects(url):
    people = {}
    pages_url_generator = get_pages_url(url)
    for page_url in pages_url_generator:
        page = requests.get(page_url)
        soup = BeautifulSoup(page.text, 'html.parser')
        rows = soup.find("tbody", {"id": "posts"}).find_all("tr", {"class": "post"})
        for row in rows:
            columns = row.find_all("td", recursive=False)
            name = re.sub(r"\s+", " ", columns[0].get_text().strip()).removesuffix(".")
            addresses = list(filter(None, parse_addresses(columns[1])))
            nationality = (
                columns[2]
                .get_text()
                .strip()
                .replace("Lanca", "Lanka")
                .replace("Bolivarian Republic of Venezuela", "VENEZUELA, BOLIVARIAN REPUBLIC OF")
                .replace("St.", "Saint")
                .replace("Kyrgz Republic", "KYRGYZSTAN")
                .replace("USA United States of America", "United States of America")
            )
            start_date = parse_date(columns[3].get_text().strip())
            end_date = parse_date(columns[4].get_text().strip())
            pub_date = parse_date(columns[5].get_text().strip())
            details = f"{columns[6].get_text().strip()}\n{columns[7].get_text().strip()}"
            name_lower = name.lower()
            if not name_lower in people:
                people[name_lower] = {"name": name, "legal_addresses": [], "nationalities": [], "specifications": []}
            people[name_lower]["specifications"].append({"start_date": start_date, "end_date": end_date, "details": details, "pub_date": pub_date})
            people[name_lower]["legal_addresses"].extend(addresses)
            people[name_lower]["nationalities"].append(nationality)
    return people


def create_objects(people):
    icle_sqlite = IcleSqlite(feature_code, config=icle_config)
    logger.log(logger.LogLevels.INFO, f"Looping through dictionary to save the elements")
    for person_dictionary in people.values():
        logger.log(logger.LogLevels.INFO, f"Saving element {str(person_dictionary)}")
        person = icle_sqlite.create_person()
        person.full_name = person_dictionary["name"]
        for address in person_dictionary["legal_addresses"]:
            person.add_address(address, '', '', '', icle_config.authority_country, "LEGAL ADDRESS")
        for nationality in person_dictionary["nationalities"]:
            person.add_nationality(nationality)
        for person_specification in person_dictionary["specifications"]:
            specification = icle_sqlite.create_specification()
            specification.add_person(person)
            specification.start_date = person_specification["start_date"]
            specification.end_date = person_specification["end_date"]
            specification.add_rationale(person, "Ineligible Entities", person_specification["details"])
    logger.log(logger.LogLevels.INFO, f"Saving file")
    icle_sqlite.save(f"./{execution_result_path}")


def main():
    people = parse_objects(icle_config.link)
    create_objects(people)


if __name__ == "__main__":
    DEBUG = False
    feature_code = "25497"
    current_folder = os.getcwd()
    execution_result_path = sys.argv[1] if len(sys.argv) > 1 else "./"
    logger = IcleLogger(feature_code, directory=f"./{execution_result_path}", separate_folders_by_day=False)
    icle_config = IcleConfig(path=f"{current_folder}/script.config", icle_logger=logger)
    logger.log(logger.LogLevels.INFO, "---- Start script ----")
    main()
    logger.log(logger.LogLevels.INFO, "---- End script ----")
