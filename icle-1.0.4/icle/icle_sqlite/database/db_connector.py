import sqlite3, os
import datetime
from shutil import copyfile

class DbConnector():
    
    def __init__(self, path):
        parameters_directory_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        parameters_file_name = "database/icle.sqlite"
        parameters_file_path = os.path.join(parameters_directory_path, parameters_file_name)
        copyfile(parameters_file_path, path)
        self.db = sqlite3.connect(path)
    
    def __execute_query(self, db_cursor, query):
        try:
            db_cursor.execute(query)
            return db_cursor
        except Exception as _:
            return None
    
    def string_to_db(self, string):
        return string.replace("'", "''")
    
    def get_element_from_db(self, values, db_columns, table_name, no_case):
        try:
            conditions = []
            for index, value in enumerate(values):
                if value is not None:
                    conditions.append("{}.{} = '{}'".format(table_name, db_columns[index], self.string_to_db(value)))
                else:
                    conditions.append("{}.{} is NULL".format(table_name, db_columns[index]))
            
            query = "SELECT *"\
                " FROM {}"\
                " WHERE {}".format(table_name, " AND ".join(conditions))
            if no_case:
                query += " COLLATE NOCASE"
            return self.__execute_query(self.db.cursor(), query).fetchone()
        except Exception as _:
            return None
    
    def get_element_id_from_db(self, values, db_columns, table_name, no_case=False):
        element = self.get_element_from_db(values, db_columns, table_name, no_case)
        if element is not None:
            return element[0]
        else:
            return None
        
    def execute_command(self, command):
        try:
            cursor = self.db.cursor()
            cursor.execute(command)
            self.db.commit()
            return cursor.lastrowid
        except Exception as _:
            return None
        
    def close(self):
        self.db.close()