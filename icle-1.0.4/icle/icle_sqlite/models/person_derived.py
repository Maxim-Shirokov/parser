import sys, os
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error

class PersonDerived(DbModel):
    
    def __init__(self):
        super().__init__()
        self.person = None