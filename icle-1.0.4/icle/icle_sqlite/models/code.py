import sys, os
from enum import Enum
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.person_derived import PersonDerived #pylint: disable=import-error
from models.database_model import DbModel #pylint: disable=import-error

class CodeType(DbModel):
    
    class DbFields(Enum):
        name = "code_name"
    
    def __init__(self):
        super().__init__()
        self.name = None 
    
    def save(self, db):
        command = self.insert_command("code_type", 
                [self.DbFields.name.value],
                [self.name])
        super().save(db, command)
    
class Code(PersonDerived):
    assignable_source_id = 1
    
    class DbFields(Enum):
        code = "code"
        person_id = "person_id"
        code_type_id = "code_type_id"
    
    def __init__(self):
        super().__init__()
        self.value = ""
        self.code_type = None
            
    def save(self, db):
        #TODO Save the code type
        self.code_type.save(db)
        command = self.insert_command("code", 
                                   [self.DbFields.code.value, self.DbFields.person_id.value, self.DbFields.code_type_id.value], 
                                   [self.value, self.person.id, self.code_type.id])
        super().save(db, command)
        