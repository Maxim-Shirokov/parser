from enum import Enum
import sys, os
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error
from models.country import Country #pylint: disable=import-error

class Source(DbModel):
    
    class DbFields(Enum): 
        name = "name"
        country_id = "country_id"
    
    def save(self, db):
        command = self.insert_command("source",
                [self.DbFields.name.value, self.DbFields.country_id.value], 
                [self.name, self.country.id if self.country else None])
        super().save(db, command)
        
    def __init__(self, all_countries = []):
        super().__init__()
        self.name = None
        self.country = None    