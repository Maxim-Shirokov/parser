import sys, os, pandas
from enum import Enum
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel #pylint: disable=import-error
from models.person_derived import PersonDerived #pylint: disable=import-error

class Nationality(DbModel):
    class CsvFields(Enum):
        value = "nationality"
    
    class DbFields(Enum):
        id = "id"
        value = "nationality"
    
    def __init__(self):
        super().__init__()
        self.name = None
    
    def save(self, db):
        command = self.insert_command("nationality", 
                                    [Nationality.DbFields.value.value],
                                    [self.name])
        super().save(db, command)

class NationalityPerson(PersonDerived):
    assignable_source_id = 1
    
    class DbFields(Enum):
        nationality_id = "nationality_id"
        person_id = "person_id"
    
    def __init__(self):
        super().__init__()
        self.nationality = None
        
    def save(self, db):
        self.nationality.save(db)
        command = self.insert_command("nationality_person", 
                                [NationalityPerson.DbFields.nationality_id.value, NationalityPerson.DbFields.person_id.value],
                                [self.nationality.id, self.person.id])
        super().save(db, command)
    