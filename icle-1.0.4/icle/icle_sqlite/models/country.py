import sys, os
from enum import Enum
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.database_model import DbModel  #pylint: disable=import-error

class Country(DbModel):
    class DbFields(Enum):
        name = "name"
    
    def __init__(self):
        super().__init__()
        self.__name = None
            
    def set_id(self, db_connector):
        self.id = db_connector.get_element_id_from_db([self.name], [self.DbFields.name.value], "country", no_case=True)