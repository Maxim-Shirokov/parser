import sys, os, datetime
import json
sys.path.append(os.path.realpath(os.path.dirname(__file__)))
from models.address import Address
from models.source import Source
from models.authority import Authority
from models.list import List
from models.link import Link
from models.person import Person
from models.specification import Specification, Category, Subcategory
from models.rationale import RationaleType
from models.country import Country
from database.db_connector import DbConnector
from utilities.validator import Validator


class IcleSqlite():
    def __init__(self, feature_code, config=None, auto_capitalization=True):
        self.feature_code = feature_code
        self.auto_capitalization = auto_capitalization
        self.__default_category = None
        self.__default_subcategory = None
        self.__default_source = None
        self.__default_authority = None
        self.__default_list = None
        self.__default_link_value = None
        self.__specifications = []
        self.__persons = []
        self.__lists, self.__sources, self.__authorities = [], [], []
        self.__categories, self.__subcategories = [], []
        self.__countries = []
        self.__rationale_types = []
        self.__nationalities = []
        self.__address_types = []
        self.__code_types = []
        self.__validator = Validator()
        parameters_directory_path = os.path.dirname(os.path.realpath(__file__))
        parameters_file_name = 'utilities/fixed_fields.json'
        parameters_file_path = os.path.join(parameters_directory_path, parameters_file_name)
        acceptable_fields_json = json.load(open(parameters_file_path, encoding="utf-8" ) )
        self.__acceptable_categories = acceptable_fields_json["categories"]
        self.__acceptable_subcategories = acceptable_fields_json["subcategories"]
        if config is not None:
            self.set_default_category(self.create_category(config.category) if config.category != "" else None)
            self.set_default_subcategory(self.create_subcategory(config.subcategory, self.__default_category) if config.subcategory != "" else None)
            self.set_default_source(self.create_source(config.source, config.source_country) if config.source != "" else None)
            self.set_default_authority(self.create_authority(config.authority, config.authority_country, authority_acronym=config.authority_acronym) if config.authority != "" else None)
            self.set_default_list(self.create_list(config.list, self.__default_authority) if config.list != "" else None)
            self.set_default_link(config.link)
    
      
    def __get_rationale_types(self):
        return self.__rationale_types
    
    def set_default_link(self, link):
        if link is not None:
            if len(link.strip()) > 0:
                self.__default_link_value = link
                return self.__default_link_value
        self.__default_link_value = None
        
    def set_default_source(self, source):
        if source not in self.__sources:
            raise Exception("Source not found")
        self.__default_source = source
        return self.__default_source
    
    def set_default_subcategory(self, subcategory):
        self.__default_subcategory = subcategory
        
        if self.__default_category:
            self.__default_subcategory.category = self.__default_category
        return self.__default_subcategory
    
    def set_default_category(self, category):
        if category not in self.__categories:
            raise Exception("Category not found")
        self.__default_category = category
        return self.__default_category
    
    def set_default_authority(self, authority):
        if authority not in self.__authorities:
            raise Exception("Authority not found")
        self.__default_authority = authority
        return self.__default_authority
    
    
    def set_default_list(self, _list):
        if not _list:
            raise Exception("List must be specified")
        self.__default_list = _list
        return self.__default_list
    
    def create_category(self, category_name):
        if category_name.upper() not in self.__acceptable_categories:
            raise Exception("Category is not valid")
        category = Category()
        category.name = category_name.upper()
        self.__categories.append(category)
        return category

    def create_subcategory(self, subcategory_name, category=None):
        if category is None and self.__default_category is None:
            raise Exception("Category must be specified")
        if subcategory_name.upper() not in self.__acceptable_subcategories:
            raise Exception("Subcategory is not valid")
        subcategory = Subcategory()
        subcategory.name = subcategory_name.upper()
        subcategory.category = category if category else self.__default_category
        self.__subcategories.append(subcategory)
        return subcategory
    
    def create_authority(self, authority_name, country_name, authority_acronym=None):
        authority = Authority()
        authority.name = authority_name
        country_parsed = self.__validator.get_actual_country_name(country_name)
        if country_parsed is None:
            raise Exception("Country {} is not acceptable".format(country_name))
        country_match = [x for x in self.__countries if x.name == country_parsed]
        if any(country_match):
            country = next(iter(country_match))
        else:
            country = Country()
            country.name = country_parsed
            self.__countries.append(country)
        authority.country = country
        authority.acronym = authority_acronym
        authorities_match = [x for x in self.__authorities if x.name == authority_name and x.country == country]
        if any(authorities_match):
            authority = next(iter(authorities_match))
        else:
            self.__authorities.append(authority)
        return authority

    def create_source(self, source_name, country_name):
        country = None
        if country_name:
            country_parsed = self.__validator.get_actual_country_name(country_name)
            if country_parsed is None:
                raise Exception("Country {} is not acceptable".format(country_name))
            country_match = [x for x in self.__countries if x.name == country_parsed]
            if any(country_match):
                country = next(iter(country_match))
            else:
                country = Country()
                country.name = country_parsed
                self.__countries.append(country)
            
        source = Source()
        source.name = source_name
        source.country = country
        sources_match = [x for x in self.__sources if x.name == source_name and x.country == country]
        if any(sources_match):
            source = next(iter(sources_match))
        else:
            self.__sources.append(source)
        return source
    
    def create_list(self, list_name, authority=None):
        _list = None
        if authority is None and self.__default_authority is None:
            raise Exception("Authority must be specified")
        
        _list = List()
        _list.name = list_name
        _list.authority = authority if authority else self.__default_authority
        lists_match = [x for x in self.__lists if x.name == list_name and x.authority == _list.authority]
        if any(lists_match):
            _list = next(iter(lists_match))
        else:
            self.__lists.append(_list)
        return _list
        
    def create_person(self, person=None):
        if person is None:
            person = Person(validator=self.__validator, auto_capitalization=self.auto_capitalization, get_nationalities=lambda: self.__nationalities, get_countries=lambda: self.__countries, get_address_types=lambda: self.__address_types, get_code_types=lambda: self.__code_types)
        self.__persons.append(person)
        return person
    
    def create_specification(self, specification=None):
        if specification is None:
            specification = Specification(self.feature_code, default_link_value=self.__default_link_value,  default_list=self.__default_list, default_source=self.__default_source, 
                                          default_authority=self.__default_authority, default_subcategory=self.__default_subcategory, 
                                          get_rationale_types=lambda: self.__rationale_types)
        self.__specifications.append(specification)
        return specification
    
    def check_for_duplicates(self):
        persons_matched = {}
        for _person in self.__persons:
            for identifier in _person._Person__identifiers:
                persons_matched = [p for p in self.__persons if p != _person and identifier in p._Person__identifiers]
                for matched in persons_matched:
                    name = (matched.first_name + " " + matched.last_name) if matched.first_name else matched.full_name
                    raise Exception(f"Person '{name}' is duplicated: Found a person with the same name and birth date")
                    
                
    
    def save(self, path):
        filename = self.feature_code + "-" + datetime.datetime.utcnow().strftime('%Y%m%d%H%M') + ".sqlite"
        _dbConnector = DbConnector(os.path.join(path, filename))
        for _country in self.__countries:
            _country.set_id(_dbConnector)
        for _authority in self.__authorities:
            _authority.save(_dbConnector)
        for _list in self.__lists:
            _list.save(_dbConnector)
        for _sources in self.__sources:
            _sources.save(_dbConnector)
        for _category in self.__categories:
            _category.save(_dbConnector)
        for _subcategory in self.__subcategories:
            _subcategory.save(_dbConnector)
        for _person in self.__persons:
            _person.save(_dbConnector)
        self.check_for_duplicates()
        for _specification in self.__specifications:
            _specification.save(_dbConnector)
            
        