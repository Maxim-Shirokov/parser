import json, os

class IcleConfig():
    @property
    def category(self):
        return self.__category
    
    @property
    def subcategory(self):
        return self.__subcategory
    
    @property
    def authority(self):
        return self.__authority
    
    @property
    def authority_country(self):
        return self.__authority_country
    
    @property
    def source(self):
        return self.__source
    
    @property
    def source_country(self):
        return self.__source_country
    
    @property
    def list(self):
        return self.__list
    
    @property
    def rationale(self):
        return self.__rationale

    @property
    def authority_acronym(self):
        return self.__authority_acronym
    
    @property
    def link(self):
        return self.__link
    
    def __init__(self, path="script.config", icle_logger=None):
        self.__category = ""
        self.__subcategory = ""
        self.__authority = ""
        self.__authority_country = ""
        self.__authority_acronym = ""
        self.__source = ""
        self.__source_country = ""
        self.__list = ""
        self.__rationale = ""
        self.__link = ""
        
        if os.path.isfile(path):
            config_dict = self.__load_config_file(path)
            if "category" in config_dict:
                self.__category = config_dict["category"]
            if "subcategory" in config_dict:
                self.__subcategory = config_dict["subcategory"]
            if "authority" in config_dict:
                self.__authority = config_dict["authority"]
            if "authority_country" in config_dict:
                self.__authority_country = config_dict["authority_country"]
            if "authority_acronym" in config_dict:
                self.__authority_acronym = config_dict["authority_acronym"]
            if "source" in config_dict:
                self.__source = config_dict["source"]
            if "source_country" in config_dict:
                self.__source_country = config_dict["source_country"]
            if "list" in config_dict:
                self.__list = config_dict["list"]
            if "rationale" in config_dict:
                self.__rationale = config_dict["rationale"]
            if "link" in config_dict:
                self.__link = config_dict["link"] 
            
        else:
            if icle_logger:
                icle_logger.log(icle_logger.LogLevels.WARN, "Config file specified at path: {} does not exist!")
    
    def __load_config_file(self, path):
        config_dict = {}
        with open(path, encoding='utf-8') as f:
            config_dict = json.load(f)
        return config_dict