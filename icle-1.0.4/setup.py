from setuptools import setup, find_packages

setup(name='icle', 
      version='1.0.4',
      author="Fabio Ceccatelli",
      author_email="f.ceccatelli@sgrcompliance.com",
      packages=find_packages(),
      include_package_data=True
)